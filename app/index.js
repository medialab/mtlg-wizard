const fs = require('fs');

let level;
let counter;

document.getElementById('settings-form').style.display = 'block';
document.getElementById('extend-form').style.display = 'none';
document.getElementById('pattern-form').style.display = 'none';
document.getElementById('error').style.display = 'none';
document.getElementById('end').style.display = 'none';
document.getElementById('hint').style.display = 'none';
document.getElementById('img-1').style.display = 'none';
document.getElementById('show-1').style.display = 'none';
document.getElementById('show-2').style.display = 'none';
document.getElementById('show-3').style.display = 'none';
document.getElementById('show-4').style.display = 'none';
document.getElementById('show-5').style.display = 'none';

document.getElementById('img-1-btn').onclick = function () {
    document.getElementById('img-1').style.display = 'block';
    document.getElementById('img-1-btn').style.display = 'none';
};

document.getElementById('show-1-btn').onclick = function () {
    document.getElementById('show-1').style.display = 'block';
    document.getElementById('show-1-btn').style.display = 'none';
};

document.getElementById('show-2-btn').onclick = function () {
    document.getElementById('show-2').style.display = 'block';
    document.getElementById('show-2-btn').style.display = 'none';
};

document.getElementById('show-3-btn').onclick = function () {
    document.getElementById('show-3').style.display = 'block';
    document.getElementById('show-3-btn').style.display = 'none';
};

document.getElementById('show-4-btn').onclick = function () {
    document.getElementById('show-4').style.display = 'block';
    document.getElementById('show-4-btn').style.display = 'none';
};

document.getElementById('show-5-btn').onclick = function () {
    document.getElementById('show-5').style.display = 'block';
    document.getElementById('show-5-btn').style.display = 'none';
};

document.getElementById('settings-button').onclick = function () {
    document.getElementById('settings-form').style.display = 'none';
    document.getElementById('pattern-form').style.display = 'block';
    document.getElementById('hint').style.display = 'block';
    level = document.getElementById('levelCount').value;
    counter = level;
    document.getElementById(
        'levelTitle'
    ).innerHTML = `<strong>Level 1</strong>`;
};

document.getElementById('extend-button').onclick = function () {
    document.getElementById('extend-button').style.display = 'none';
    document.getElementById('extend-form').style.display = 'block';
};

document.getElementById('pattern-button').onclick = function () {
    document.getElementById('error').style.display = 'none';
    if (
        document.getElementById('text-1656622574823').value == '' ||
        document.getElementById('text-1656622641909').value == '' ||
        document.getElementById('text-1656622641540').value == '' ||
        document.getElementById('text-1656622641141').value == '' ||
        document.getElementById('text-1656624539394').value == '' ||
        document.getElementById('text-1656624572175').value == '' ||
        document.getElementById('text-1656624653162').value == '' ||
        document.getElementById('text-1656624651786').value == '' ||
        document.getElementById('text-1656624867766').value == ''
    ) {
        console.error('Invalid configuration');
        document.getElementById('error').style.display = 'block';
        return;
    }

    let currentLevel = level - counter + 2;
    const levelName = `level${currentLevel - 2}`;

    const patternArray = [
        document.getElementById('text-1656622574823').value,
        document.getElementById('text-1656622641909').value,
        document.getElementById('text-1656622641540').value,
        document.getElementById('text-1656622641141').value,
    ];
    pattern[levelName] = [removeEmpty(patternArray)];

    const wordsArray = [
        document.getElementById('text-1656624539394').value,
        document.getElementById('text-1656624573363').value,
        document.getElementById('text-1656624570121').value,
        document.getElementById('text-1654445879215').value,
        document.getElementById('text-1656625497682').value,

        document.getElementById('text-1656624572175').value,
        document.getElementById('text-1656624554986').value,
        document.getElementById('text-1656624555861').value,
        document.getElementById('text-1656624564975').value,
        document.getElementById('text-1656624575219').value,

        document.getElementById('text-1656624653162').value,
        document.getElementById('text-1656624651402').value,
        document.getElementById('text-1656624651011').value,
        document.getElementById('text-1656624688796').value,
        document.getElementById('text-1656624610025').value,

        document.getElementById('text-1656624651786').value,
        document.getElementById('text-1656624653474').value,
        document.getElementById('text-1656624714531').value,
        document.getElementById('text-1656624688799').value,
        document.getElementById('text-1656624745454').value,
    ];

    const nonMatchingArray = [
        document.getElementById('text-1656624867766').value,
        document.getElementById('text-1656624652134').value,
        document.getElementById('text-1656624879456').value,
        document.getElementById('text-1656624857691').value,
        document.getElementById('text-1656624248777').value,
    ];

    words[levelName] = [
        {
            matching: removeEmpty(wordsArray),
            nonMatching: removeEmpty(nonMatchingArray),
        },
    ];

    solution['default'].push({
        pattern: document.getElementById('text-1656622574823').value,
        words: removeEmpty([
            document.getElementById('text-1656624539394').value,
            document.getElementById('text-1656624573363').value,
            document.getElementById('text-1656624570121').value,
            document.getElementById('text-1654445879215').value,
            document.getElementById('text-1656625497682').value,
        ]),
    });

    solution['default'].push({
        pattern: document.getElementById('text-1656622641909').value,
        words: removeEmpty([
            document.getElementById('text-1656624572175').value,
            document.getElementById('text-1656624554986').value,
            document.getElementById('text-1656624555861').value,
            document.getElementById('text-1656624564975').value,
            document.getElementById('text-1656624575219').value,
        ]),
    });

    solution['default'].push({
        pattern: document.getElementById('text-1656622641540').value,
        words: removeEmpty([
            document.getElementById('text-1656624653162').value,
            document.getElementById('text-1656624651402').value,
            document.getElementById('text-1656624651011').value,
            document.getElementById('text-1656624688796').value,
            document.getElementById('text-1656624610025').value,
        ]),
    });

    solution['default'].push({
        pattern: document.getElementById('text-1656622641141').value,
        words: removeEmpty([
            document.getElementById('text-1656624651786').value,
            document.getElementById('text-1656624653474').value,
            document.getElementById('text-1656624714531').value,
            document.getElementById('text-1656624688799').value,
            document.getElementById('text-1656624745454').value,
        ]),
    });

    if (counter > 1) {
        document.getElementById(
            'levelTitle'
        ).innerHTML = `<strong>Level ${currentLevel}</strong>`;
        counter--;

        //Clear input fields for next level
        const clearInputs = document.getElementsByClassName('clear');
        Array.from(clearInputs).forEach((element) => {
            element.value = '';
        });
    } else {
        document.getElementById('pattern-form').style.display = 'none';
        document.getElementById('hint').style.display = 'none';
        document.getElementById('end').style.display = 'block';

        const settings = {
            default: {
                gameName: document.getElementById('select-1666473423666').value,
                bgImage: `background/${
                    document.getElementById('select-1666415796338').value
                }`,
                bgPath: '',
                regExToString: regExToString,
                verifyFunction: verifyFunction,
                gameMode: 'cooperative',
                useConstant: true,
                nbrWords: 25,
                timerMaxTime:
                    parseInt(document.getElementById('timerMaxTime').value) *
                    1000,
                delayWords: 0,
                plusScore: parseInt(document.getElementById('plusScore').value),
                minusScore: parseInt(
                    document.getElementById('minusScore').value
                ),
                alphabet: ['words'],
                cooperativeRegEx: pattern,
                cooperativeWords: words,
                wordView: 'pictures',
            },
            design: {
                words: {
                    font: document.getElementById('select-1666473423562').value,
                    fontType: 'bold',
                    fontColor: document.getElementById('select-1666473788434')
                        .value,
                    background: 'white',
                },
                pattern: {
                    font: document.getElementById('select-1666473423562').value,
                    fontType: 'bold',
                    fontColor: 'rgb(243,242,242)',
                    fontBackground: 'rgba(0, 0, 0, 0.8)',
                    fontAlign: 'center',
                    background: 'rgba(0, 0, 0, 0.3)',
                },
            },
            all: {
                modes: [
                    {
                        key: 'cooperative',
                        note: 'Different regEx, one score, timer, four level.',
                    },
                ],
            },
        };

        fs.writeFile(
            `./words/dev/js/game/games/presets/settingsWizard_${
                document.getElementById('select-1666473423666').value
            }.js`,
            '',
            function () {
                console.log('File cleaned. Now writing...');
            }
        );

        const ws = fs.createWriteStream(
            `./words/dev/js/game/games/presets/settingsWizard_${
                document.getElementById('select-1666473423666').value
            }.js`,
            {
                flags: 'a',
            }
        );

        ws.write(
            `function getSettings_${
                document.getElementById('select-1666473423666').value
            } () {`
        );
        ws.write('\n\nconst solution = ');
        ws.write(JSON.stringify(solution, null, 2));

        //Special type of stringify to preserve function references in JSON object
        ws.write('\n\nconst settings = ');
        ws.write(
            JSON.stringify(settings, jsonReplacer, 2).replace(
                /"\{func_(\d+)\}"/g,
                funcReplacer
            )
        );

        ws.write('\n\nreturn settings;\n\n}\n');
    }
};

const pattern = {};

const words = {};

const solution = {
    default: [],
};

const verifyFunction = function (word, pattern) {
    const _solution = solution.default;

    function search(el) {
        if (el.pattern === pattern) {
            return el.words.includes(word);
        }
        return false;
    }

    if (_solution.find(search) !== undefined) {
        return true;
    }

    return false;
};

const regExToString = function (regEx) {
    return regEx;
};

function removeEmpty(arr) {
    return arr.filter(function (el) {
        return el != '';
    });
}

//https://stackoverflow.com/a/50009332
const functions = [];

const jsonReplacer = function (key, val) {
    if (typeof val === 'function') {
        functions.push(val.toString());

        return '{func_' + (functions.length - 1) + '}';
    }

    return val;
};

const funcReplacer = function (match, id) {
    return functions[id];
};

/*document.getElementById('grafics-button').onclick = function () {
    console.log(document.getElementById('image-file').files[0])
    const file = document.getElementById('image-file').files[0];
    fs.writeFileSync(`./words/dev/img/games/${file.name}`, file);
    window.open("file://C:/Users/meien/Downloads/")
};*/
