# MTLG Wizard

Setup wizard to create multitouch learning games for non-tech savvy users.

## Setup

### Download and Install

```
git clone git@git.rwth-aachen.de:medialab/mtlg-wizard.git
npm install
```

## Start Wizard

```
npm run wizard
```

## Games

### Words

#### Start Game

```
npm run words-game
```

#### Export Package File

```
npm run words-package
```

### Internet

#### Start Game

```
npm run internet-game
```

#### Export Package File

```
npm run internet-package
```
