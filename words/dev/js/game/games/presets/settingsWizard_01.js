function getSettings_01 () {

const solution = {
  "default": [
    {
      "pattern": "33",
      "words": [
        "33",
        "33"
      ]
    },
    {
      "pattern": "33",
      "words": [
        "33",
        "33"
      ]
    },
    {
      "pattern": "33",
      "words": [
        "33",
        "33"
      ]
    },
    {
      "pattern": "33",
      "words": [
        "33",
        "33"
      ]
    }
  ]
}

const settings = {
  "default": {
    "gameName": "01",
    "bgImage": "background/RegEx.jpg",
    "bgPath": "",
    "regExToString": function (regEx) {
    return regEx;
},
    "verifyFunction": function (word, pattern) {
    const _solution = solution.default;

    function search(el) {
        if (el.pattern === pattern) {
            return el.words.includes(word);
        }
        return false;
    }

    if (_solution.find(search) !== undefined) {
        return true;
    }

    return false;
},
    "gameMode": "cooperative",
    "useConstant": true,
    "nbrWords": 25,
    "timerMaxTime": 90000,
    "delayWords": 0,
    "plusScore": 2,
    "minusScore": -1,
    "alphabet": [
      "words"
    ],
    "cooperativeRegEx": {
      "level0": [
        [
          "33",
          "33",
          "33",
          "33"
        ]
      ]
    },
    "cooperativeWords": {
      "level0": [
        {
          "matching": [
            "33",
            "33",
            "33",
            "33",
            "33",
            "33",
            "33",
            "33"
          ],
          "nonMatching": [
            "33",
            "33"
          ]
        }
      ]
    },
    "wordView": "pictures"
  },
  "design": {
    "words": {
      "font": "Arial",
      "fontType": "bold",
      "fontColor": "blue",
      "background": "white"
    },
    "pattern": {
      "font": "Arial",
      "fontType": "bold",
      "fontColor": "rgb(243,242,242)",
      "fontBackground": "rgba(0, 0, 0, 0.8)",
      "fontAlign": "center",
      "background": "rgba(0, 0, 0, 0.3)"
    }
  },
  "all": {
    "modes": [
      {
        "key": "cooperative",
        "note": "Different regEx, one score, timer, four level."
      }
    ]
  }
}

return settings;

}
