/**
 * @Date:   2017-10-18T12:14:16+02:00
 * @Last modified time: 2018-03-14T15:25:11+01:00
 */

var field_init = function()
{
  // MTLG.createContainers kreirt ein neuen Container welche die bereits
  // vorhande Stage überdeckt
  // Die Funktion drawField zeichnet das Spielfeld mittels den Container
  areas = MTLG.createContainers(1);
  drawField(areas[0]);
  MTLG.getStage().addChild(areas[0]);
};

/*
* Wichtige Variablen für die Elemente des Spielfeldes
*/
var touchDevice = true; //Gibt an ob das Gerät Touch besitzt
var taskLevel = 0;  //Schwierigkeitsstufe der Aufgaben 0 = Mix | 1 = easy | 2  = hard
var character = [];  //Zugriff auf die Spielfiguren
var xField = [];    //x-Koordinaten der Felder - Koordinaten werden für die Bewegung der Figuren benötigt
var yField = [];    //y-Koordinate der Felder
var fieldTaken = new Array(14).fill(0);  //Speichert die Anzahl an  Spielern auf einem Feld
var playerPosition = [];  //Speichert die aktuelle Position der Spielfiguren
var playerOffset = []; //Speichert die gelaufenen Felder eines Spielers
var playerTurn;     //Gibt an welcher Spieler an der Reihe ist
var turnArrow;      //Pfeil der auf den aktuellen Spieler zeigt
var playerCounter;  //Wird für die Rotation der Spieler benötigt
var figureArray = [];  //Wird für das Feedbackmodul benötigt, Anzeige von Flair auf den Robotern
var createTutorial;   //Erzeugt die Hinweise auf dem Spielfeld
var tutorialOnce;     //Tutorialfunktion soll nur einmal ausgeführt werden

/*
* Die Funktion drawField zeichnet das Spielfeld mit den übergebenen Bereich
* @param area: bereich in dem gezeichnet wird
*/
var drawField = function(area)
{
    var stage = MTLG.getStage();

    //Hintergrund
    var background = MTLG.assets.getBitmap("img/mainMenu/mainBackground.png");
    var coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
    background.regX = background.getBounds().width*0.5;
    background.regY = background.getBounds().height*0.5;
    background.x = coordinates.xCoord;
    background.y = coordinates.yCoord;
    stage.addChild(background);

    //Einstellungsvariablen für das Spielfeld
    var numberOfPlayers = MTLG.getPlayerNumber();
    var numberOfField = 14;

    //Erstelle das Spielbrett mit den jeweiligen Settings
    var gameboard = new GameBoard(area, numberOfPlayers, numberOfField, taskLevel);

    //Erstelle zurück zum Hauptmenü Button
    var menuButton = MTLG.assets.getBitmap("img/gameboard/menuButton.png");
    menuButton.regX = menuButton.getBounds().width*0.5;
    menuButton.regY = menuButton.getBounds().height*0.5;
    menuButton.x = area.getBounds().width/2;
    menuButton.y = area.getBounds().height/2.8;
    //Erstelle eine Bestätigungsabfrage
    menuButton.on("click", function(event){
        //Entferne Hinweise von Feedback von der Area
        MTLG.utilities.fbm.removeAllFeedback();
        area.addChild(confirmMenuContainer);
    });
    area.addChild(menuButton);

    //Bestätigungsabfrage
    var confirmCover = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
    confirmCover.on('click', function(event){
        //Overlay welches clickevents abfängt und sonst nichts macht
    });
    var confirmMenu = MTLG.assets.getBitmap("img/gameboard/confirmMenu.png");
    confirmMenu.regX = confirmMenu.getBounds().width*0.5;
    confirmMenu.regY = confirmMenu.getBounds().height*0.5;
    confirmMenu.x = area.getBounds().width/2;
    confirmMenu.y = area.getBounds().height/2;
    var templateConfirm = l("back_to_menu");
    var textConfirm = new createjs.Text(templateConfirm, '50px Times New Roman', 'black');
    textConfirm.regX = textConfirm.getBounds().width/2;
    textConfirm.regY = textConfirm.getBounds().height/2;
    textConfirm.x = confirmMenu.x;
    textConfirm.y = confirmMenu.y - 70;

    //Ja Button vom der Bestätigungsabfrage
    var yesButton = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/yes.png", 0.7, 0.7);
    yesButton.regX = yesButton.getBounds().width*0.5;
    yesButton.regY = yesButton.getBounds().height*0.5;
    yesButton.x = confirmMenu.x - 120;
    yesButton.y = confirmMenu.y + 60;
    yesButton.on("click", function(event){
        //Kehre zurück ins Hauptmenü
        MTLG.lc.goToMenu();
    });

    //Nein Button vom der Bestätigungsabfrage
    var noButton = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/no.png", 0.7, 0.7);
    noButton.regX = noButton.getBounds().width*0.5;
    noButton.regY = noButton.getBounds().height*0.5;
    noButton.x = confirmMenu.x + 120;
    noButton.y = confirmMenu.y + 60;
    noButton.on("click", function(event){
        //Entferne die Abfrage wieder
        area.removeChild(confirmMenuContainer);
    });

    var confirmMenuContainer = new createjs.Container();
    confirmMenuContainer.addChild(confirmCover, confirmMenu, textConfirm, yesButton, noButton);


    //Anzeige für den aktuellen Spieler
    var turnCircle = MTLG.assets.getBitmap("img/gameboard/turnCircle.png");
    turnCircle.regX = turnCircle.getBounds().width*0.5;
    turnCircle.regY = turnCircle.getBounds().height*0.5;
    turnCircle.x = area.getBounds().width/2;
    turnCircle.y = area.getBounds().height/2;
    //Lade die Farbe des jeweiligen Spielers für den Pfeil
    var arrow1 = MTLG.assets.getBitmap("img/gameboard/arrow1.png");
    arrow1.visible = true;
    var arrow2 = MTLG.assets.getBitmap("img/gameboard/arrow2.png");
    arrow2.visible = false;
    var arrow3 = MTLG.assets.getBitmap("img/gameboard/arrow3.png");
    arrow3.visible = false;
    var arrow4 = MTLG.assets.getBitmap("img/gameboard/arrow4.png");
    arrow4.visible = false;
    turnArrow = new createjs.Container();
    turnArrow.addChild(arrow1, arrow2, arrow3, arrow4);
    turnArrow.regX = turnArrow.getBounds().width*0.5;
    turnArrow.regY = turnArrow.getBounds().height*0.5;
    turnArrow.x = area.getBounds().width/2;
    turnArrow.y = area.getBounds().height/2;
    turnArrow.rotation = 240;   // Zu Spielbeginnt zeigt dieser immer auf den grauen Roboter

    area.addChild(turnCircle, turnArrow);


    //Schwarze Abdeckung
    var blackCoverFB = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
    blackCoverFB.addEventListener('click', function(event){
        //Overlay welches clickevents abfängt und sonst nichts macht
    });
    area.addChild(blackCoverFB);

    //Entfernt den schwarzen Overlay
    function removeCover (){
      area.removeChild(blackCoverFB);
    }

    //Erstellt die Hinweise für Karten und Pfeil
    createTutorial = function(){
      feedbackArrow();
      feedbackCard();
    }

    //Erstellt den Hinweis für den Pfeil
    function feedbackArrow(){
      MTLG.utilities.fbm.giveFeedback("pointerPos", "pointerHint", "tutorTiming", "player"+playerTurn);
    }

    //Erstellt die Hinweise für Aufgabenkarten passend zu dem Schwierigkeitsgrad und der Rotation
    function feedbackCard(){
        if(touchDevice){
            if(taskLevel == 0){
                MTLG.utilities.fbm.giveFeedback("cardTargetLeft", "easyCards", "tutorTiming", "player"+playerTurn);
                MTLG.utilities.fbm.giveFeedback("cardTargetRight", "hardCards", "tutorTiming", "player"+playerTurn);
            } else
            if(taskLevel == 1){
                MTLG.utilities.fbm.giveFeedback("cardTargetLeft", "easyCards", "tutorTiming", "player"+playerTurn);
                MTLG.utilities.fbm.giveFeedback("cardTargetRight", "easyCards", "tutorTiming", "player"+playerTurn);
            } else {
              MTLG.utilities.fbm.giveFeedback("cardTargetLeft", "hardCards", "tutorTiming", "player"+playerTurn);
              MTLG.utilities.fbm.giveFeedback("cardTargetRight", "hardCards", "tutorTiming", "player"+playerTurn);
            }
        } else {
            if(taskLevel == 0){
                MTLG.utilities.fbm.giveFeedback("cardTutLeft", "easyCards", "tutorTiming", "player"+playerTurn);
                MTLG.utilities.fbm.giveFeedback("cardTutRight", "hardCards", "tutorTiming", "player"+playerTurn);
            } else
            if(taskLevel == 1){
                MTLG.utilities.fbm.giveFeedback("cardTutLeft", "easyCards", "tutorTiming", "player"+playerTurn);
                MTLG.utilities.fbm.giveFeedback("cardTutRight", "easyCards", "tutorTiming", "player"+playerTurn);
            } else {
              MTLG.utilities.fbm.giveFeedback("cardTutLeft", "hardCards", "tutorTiming", "player"+playerTurn);
              MTLG.utilities.fbm.giveFeedback("cardTutRight", "hardCards", "tutorTiming", "player"+playerTurn);
            }
        }
    }
    //Hinweise sollen nur einmal getriggert werden
    tutorialOnce = true;

    //Timingobj. müssen lokal wegen der callback erzeugt werden
    var goalTiming = MTLG.utilities.fbm.createTiming("goalTiming");
    goalTiming.totalDuration = 120;
    goalTiming.openDuration = 30;
    goalTiming.requestable = false;
    goalTiming.closeable = true;
    goalTiming.minimizable = false;
    goalTiming.movable = false;

    var hintTiming = MTLG.utilities.fbm.createTiming("hintTiming");
    hintTiming.totalDuration = 120;
    hintTiming.openDuration = 30;
    hintTiming.requestable = false;
    hintTiming.closeable = true;
    hintTiming.minimizable = false;
    hintTiming.movable = false;
    hintTiming.closeCallback = function(){
      if(tutorialOnce){
          tutorialOnce = false;
          removeCover();
          createTutorial();
      }
    };

    //Touch Abfrage für die Feedbackelemente
    if(touchDevice == false){
        MTLG.utilities.fbm.giveFeedback("centerTarget", "gameHint", "hintTiming", "mcStyle");
        MTLG.utilities.fbm.giveFeedback("centerTarget", "gameGoal", "goalTiming", "mcStyle");
    }else{
        MTLG.utilities.fbm.giveFeedback("mcTargetBot", "gameHint", "hintTiming", "mcStyle");
        MTLG.utilities.fbm.giveFeedback("mcTargetTop", "gameHint", "hintTiming", "mcStyle");
        MTLG.utilities.fbm.giveFeedback("mcTargetBot", "gameGoal", "goalTiming", "mcStyle");
        MTLG.utilities.fbm.giveFeedback("mcTargetTop", "gameGoal", "goalTiming", "mcStyle");
    }

    //Anleitungsvariable für die Spieltypen
    firstTimeMC = numberOfPlayers;
    firstTimePair = numberOfPlayers;
};


//Konstruktor für Spielfigur und Startfeld
function Character(playerNumber, xCoor, yCoor, fieldBool,){
    this.Container_constructor();

    this.setup(playerNumber, xCoor, yCoor, fieldBool);
}

var c = createjs.extend(Character, createjs.Container);

c.setup = function(playerNumber, xCoor, yCoor, fieldBool){

    //Ein Computer als Startfeld
    var pc = MTLG.assets.getBitmap("img/gameboard/pc"+playerNumber+".png");
    pc.regX = pc.getBounds().width*0.5;
    pc.regY = pc.getBounds().height*0.5;
    pc.x = xCoor;
    pc.y = yCoor;
    pc.visible = false;

    //Überprüfe ob Startfeld benötigt wird
    if(fieldBool){pc.visible = true;}

    //Erstelle Spielfigur
    var figure = MTLG.assets.getBitmap("img/gameboard/player"+playerNumber+".png");
    var figureBounds = figure.getBounds();
    var figureContainer = new createjs.Container();
    figureContainer.addChild(figure);
    figureContainer.regX = figure.getBounds().width*0.5;
    figureContainer.regY = figure.getBounds().height*0.5;
    figureContainer.x = xCoor;
    figureContainer.y = yCoor;
    figureContainer.feedbackY = -0.07;
    figureArray[playerNumber] = figureContainer;
    figureContainer.on("pressmove", function(event){
      figureContainer.setBounds(figure.x-figure.regX, figure.y-figure.regY, figureBounds.width, figureBounds.height);
      MTLG.utilities.fbm.giveFeedback(figureContainer, "TriedMoving")
    });
    figureContainer.on("mousedown", function(event){
      MTLG.lightParse.matchEvent({user: playerNumber, action : "TriedMoving"});
    });
    //Abfrage für Multitouch-Displays, rotiere entsprechend
    if(touchDevice == true && (playerCounter == 2 || playerCounter == 3)){
      pc.rotation = 180;
      figureContainer.rotation = 180;
    }
    if(touchDevice == true && playerCounter == 1){
      figureContainer.rotation = 360;
    }
    playerCounter -= 1 ;
    this.addChild(pc, figureContainer);
}

window.Character = createjs.promote(Character, "Container");


//Konstruktor für die Spielfelder
function Field(xCoor, yCoor){
    this.Container_constructor();

    this.setup(xCoor, yCoor);
}

var f = createjs.extend(Field, createjs.Container);

f.setup = function(xCoor, yCoor){

    //Erstelle Feld
    var imgField = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/field.png", 0.95, 0.95);
    imgField.regX = imgField.getBounds().width*0.5;
    imgField.regY = imgField.getBounds().height*0.5;
    imgField.x = xCoor;
    imgField.y = yCoor;
    //window.imgField = imgField;

    this.addChild(imgField);
}

window.Character = createjs.promote(Field, "Container");


//Konstruktor für die Aufgabenkarten
function TaskCard(area, level, xCoor, yCoor, numberOfPlayers){
    this.Container_constructor();

    this.setup(area, level, xCoor, yCoor, numberOfPlayers);
}

var t = createjs.extend(TaskCard, createjs.Container);

t.setup = function(area, level, xCoor, yCoor, numberOfPlayers){

    //Erstelle die benötigte Spielkarte
    var card;
    if(level == "easy"){
      card = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/easyCard.png", 0.9, 0.9);
    } else
    if(level == "hard"){
      if(taskLevel == 2){
        card = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/hardCard1Step.png", 0.9, 0.9);
      }else{
        card = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/hardCard.png", 0.9, 0.9);
      }
    } else {console.log("Level Parameter falsch");}
    card.regX = card.getBounds().width*0.5;
    card.regY = card.getBounds().height*0.5;
    card.x = xCoor;
    card.y = yCoor;


    this.addChild(card);


    //Anklicken der Karte erzeugt Aufgabe
    this.on("click", function(event){
      var newTask = new Task(area, level, numberOfPlayers);
    });
}

window.TaskCard = createjs.promote(TaskCard, "Container");


/**
  * Konstruktor für das Spielfeld, welches die vorangegangen Konstrukoren nutzt
  * @param area: Bereich in dem gezeichnet wird
  * @param numberOfPlayers: Anzahl der Spieler
  * @param numberOfField: Anzahl der Spielfelder
  * @param taskLevel: Schwierigkeitsstufe der Aufgaben[0 - leicht und schwere | 1 - nur leichte | 2 - nur schwere]
  */
function GameBoard(area, numberOfPlayers, numberOfField, taskLevel){
    this.Container_constructor();

    this.setup(area, numberOfPlayers, numberOfField, taskLevel);
}

var g = createjs.extend(GameBoard, createjs.Container);

g.setup = function(area, numberOfPlayers, numberOfField, taskLevel){

    //Koordinaten Datenbank für die Elemente des Spielfeldes
    //Koordinaten für die Spielfiguren
    var xChar = [];
        xChar[0] = area.getBounds().width*0.08,
        xChar[1] = area.getBounds().width*0.08,
        xChar[2] = area.getBounds().width*0.92,
        xChar[3] = area.getBounds().width*0.92;
    var yChar = [];
        yChar[0] = area.getBounds().height*0.88,
        yChar[1] = area.getBounds().height*0.12,
        yChar[2] = area.getBounds().height*0.12,
        yChar[3] = area.getBounds().height*0.88;

    //Koordinaten für die Felder des Spielbrettes
        xField[0] = area.getBounds().width*0.13,
        xField[1] = area.getBounds().width*0.1;
        xField[2] = area.getBounds().width*0.23,
        xField[3] = area.getBounds().width*0.36,
        xField[4] = area.getBounds().width*0.5,
        xField[5] = area.getBounds().width*0.64,
        xField[6] = area.getBounds().width*0.77,
        xField[7] = area.getBounds().width*0.9,
        xField[8] = area.getBounds().width*0.86,
        xField[9] = area.getBounds().width*0.77,
        xField[10] = area.getBounds().width*0.64,
        xField[11] = area.getBounds().width*0.5,
        xField[12] = area.getBounds().width*0.365,
        xField[13] = area.getBounds().width*0.23;

        yField[0] = area.getBounds().height*0.64,
        yField[1] = area.getBounds().height*0.38,
        yField[2] = area.getBounds().height*0.11,
        yField[3] = area.getBounds().height*0.19,
        yField[4] = area.getBounds().height*0.12,
        yField[5] = area.getBounds().height*0.18,
        yField[6] = area.getBounds().height*0.11,
        yField[7] = area.getBounds().height*0.38,
        yField[8] = area.getBounds().height*0.63,
        yField[9] = area.getBounds().height*0.89,
        yField[10] = area.getBounds().height*0.82,
        yField[11] = area.getBounds().height*0.9,
        yField[12] = area.getBounds().height*0.83,
        yField[13] = area.getBounds().height*0.89;


    //Erstelle die Felder des Spielbrettes
    for(var i = 0; i < numberOfField; i++){
        var field = [];
        field[i] = new Field(xField[i], yField[i]);
        this.addChild(field[i]);
        area.addChild(field[i]);
    }

    //Erstelle die Aufgabenkarten
    if(taskLevel == 0){
        var card0 = new TaskCard(area, "easy", area.getBounds().width*0.33, area.getBounds().height/2, numberOfPlayers);
        var card1 = new TaskCard(area, "hard", area.getBounds().width*0.67, area.getBounds().height/2, numberOfPlayers);
    } else
    if(taskLevel == 1){
        var card0 = new TaskCard(area, "easy", area.getBounds().width*0.33, area.getBounds().height/2, numberOfPlayers);
        var card1 = new TaskCard(area, "easy", area.getBounds().width*0.67, area.getBounds().height/2, numberOfPlayers);
    } else
    if(taskLevel == 2){
        var card0 = new TaskCard(area, "hard", area.getBounds().width*0.33, area.getBounds().height/2, numberOfPlayers);
        var card1 = new TaskCard(area, "hard", area.getBounds().width*0.67, area.getBounds().height/2, numberOfPlayers);

    } else {console.log("Ungültiger Schwierigkeitsgrad")};

    area.addChild(card0);
    area.addChild(card1);


    //Erstelle die Spielfiguren mit ihren Startfeldern
    for(var i = 0; i < numberOfPlayers; i++){
        character[i] = new Character(i, xChar[i], yChar[i], true);
        this.addChild(character[i]);
        area.addChild(character[i]);

    }
}

window.GameBoard = createjs.promote(GameBoard, "Container");


/**
 * Hilfsfunktionen zum verblassen und erscheinen von Objekten
 * @param object: das jeweilige Objekt, welches genutzt werden soll
 * @param time: benötigte Zeit in Millisekunden
 */

//Funktion zum langsamen verblassen von Objekten
function objectFadeOut(object, time){
    createjs.Tween.get(object).to({alpha: 0},time);
}

//Funktion zum langsamen erscheinen von Objekten
function objectFadeIn(object, time){
    createjs.Tween.get(object).to({alpha: 1},time);
}


/**
 * Die Funktion checkField überprüft, ob drawField das nächste Level sein soll
 * @param the current game state (an object containing only nextLevel in this case)
 * @return eine Zahl zwischen [0,1] welche die Möglichkeiten representiert
 */
var checkField = function(gameState){
  if(!gameState){
    return 0; //Starte diese Spielebene nicht ohne Grund
  }
  if(!gameState.nextLevel){
    return 0; //Starte diese Spielebene nicht ohne Grund
  }
  if(gameState.nextLevel === 2){
    return 1;
  } else {
    return 0;
  }
}
