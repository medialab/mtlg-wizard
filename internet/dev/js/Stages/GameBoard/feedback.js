/**
 * @Date:   2018-03-05T14:00:30+01:00
 * @Last modified time: 2018-03-07T10:39:45+01:00
 */

//Enthält alle Feedbackobjekte für das Feedbackmodul
var createFeedback = function()
{

  //Grüner Flair bei korrekter Lösung MC
  var correctcontent = MTLG.utilities.fbm.createContent("correctAnswer");
  correctcontent.flair="positive";
  correctcontent.flairSize=150;
  correctcontent.flairTime=2.5;
  correctcontent.flairIntensity=1;

  //Roter Flair bei falscher Lösung MC
  var wrongcontent = MTLG.utilities.fbm.createContent("wrongAnswer");
  wrongcontent.flair="negative";
  wrongcontent.flairSize=150;
  wrongcontent.flairTime=2.5;
  wrongcontent.flairIntensity=1;

  //Positver Ton
  var correctSound = MTLG.utilities.fbm.createContent("correctSound");
  correctSound.sound = "positive";

  //Negativer Ton
  var wrongSound = MTLG.utilities.fbm.createContent("wrongSound");
  wrongSound.sound = "negative";

  //Roter Flair beim versuch die Figuren zu Bewegen
  var TriedMoving = MTLG.utilities.fbm.createContent("TriedMoving");
  TriedMoving.flair="negative";
  TriedMoving.flairSize=25;
  TriedMoving.flairTime=0.2;
  TriedMoving.flairIntensity=0.5;

  //Hinweis bei 3x Versuch die Figur zu bewegen
  var TriedMoving3x = MTLG.utilities.fbm.createContent("TriedMoving3x");
  TriedMoving3x.text=l("tried_moving");
  TriedMoving3x.totalWidth=0.08;
  TriedMoving3x.totalHeight=0.08;

  var figuremoveTiming = MTLG.utilities.fbm.createTiming("figuremove");
  figuremoveTiming.closeCallback= function(){figurefeedbackdisplayed=false};
  figuremoveTiming.requestable=true;
  figuremoveTiming.minimizable=false;
  figuremoveTiming.reqPointer = false;
  figuremoveTiming.totalDuration=15;
  figuremoveTiming.openDuration=10;
  figuremoveTiming.miniDuration=10;

  //Erstelle passende farbliche Styles für alle Spielenden
  //Hinweis soll passend zur Farbe der Figuren erscheinen
  var player0style = MTLG.utilities.fbm.createStyle("player0");
  player0style.color = "#c5c5c5";
  player0style.buttonColor = "#c5c5c5";
  player0style.curve = 0.4;
  player0style.fontType = "Times New Roman";

  var player1style = MTLG.utilities.fbm.getCopyStyle("player0");
  player1style.color = "#f5f57f";
  player1style.buttonColor = "#f5f57f";
  MTLG.utilities.fbm.setStyle(player1style, "player1");

  var player2style = MTLG.utilities.fbm.getCopyStyle("player0");
  player2style.color = "#fb8a8a";
  player2style.buttonColor= "#fb8a8a"
  MTLG.utilities.fbm.setStyle(player2style, "player2");

  var player3style = MTLG.utilities.fbm.getCopyStyle("player0");
  player3style.color = "#a2ff83";
  player3style.buttonColor = "#a2ff83";
  MTLG.utilities.fbm.setStyle(player3style, "player3");

  //Pattern Matching für lightParse
  var pattern3TimesFigure = "SEQ (x1,x2,x3) WHERE (x1.user = x2.user, x2.user = x3.user, x1.action = 'TriedMoving', x2.action = 'TriedMoving', x3.action = 'TriedMoving')";
  MTLG.lightParse.addPattern(pattern3TimesFigure, figurefeedback);

  var figurefeedbackdisplayed = false;
  function figurefeedback(event){
    if (!figurefeedbackdisplayed){
      var figurefeedbackdisplayed = false;
      MTLG.utilities.fbm.giveFeedback(figureArray[event.x1.user], event.x1.action+"3x", "figuremove", "player"+event.x1.user);
    }
  }

  //Hinweise zu Beginn des Spieles

  //Hinweis von der einfachen Spielkarte
  var cardHintEasy = MTLG.utilities.fbm.createContent("easyCards");
  cardHintEasy.text = l("easy_hint");
  cardHintEasy.textX = 0;
  cardHintEasy.textY = 0;
  cardHintEasy.totalWidth = 0.15;
  cardHintEasy.totalHeight = 0.15;

  //Hinweis von der schweren Spielkarte
  var cardHintHard = MTLG.utilities.fbm.createContent("hardCards");
  cardHintHard.text=l("hard_hint");
  cardHintHard.textX=0;
  cardHintHard.textY=0;
  cardHintHard.totalWidth=0.15;
  cardHintHard.totalHeight=0.15;

  //Hinweis für den Pfeil
  var pointerHint = MTLG.utilities.fbm.createContent("pointerHint");
  pointerHint.text=l("arrow_hint");
  pointerHint.textX=0;
  pointerHint.textY=0;
  pointerHint.totalWidth=0.15;
  pointerHint.totalHeight=0.15;

  //Target für den Pfeil
  var pointerTarget = MTLG.utilities.fbm.createTarget("pointerPos");
  pointerTarget.x = 0.5;
  pointerTarget.y = 0.62;
  pointerTarget.reqPointerRotation = 180;

  //Timingobj. von der Einführung
  var tutorTiming = MTLG.utilities.fbm.createTiming("tutorTiming");
  tutorTiming.totalDuration = 999;
  tutorTiming.openDuration = 60;
  tutorTiming.miniDuration = 999;
  tutorTiming.requestable = true;
  tutorTiming.closeable = true;
  tutorTiming.minimizable = false;
  tutorTiming.movable = true;
  tutorTiming.movableMini = false;
  tutorTiming.rotateOnMove = true;
  tutorTiming.reqPointer = true;

  //Neutrales Flair
  var blueFlair = MTLG.utilities.fbm.createContent("turnFlair");
  blueFlair.flair="neutral";
  blueFlair.flairSize=25;
  blueFlair.flairTime=999;
  blueFlair.flairIntensity=1;

  //Ein Target für den Mittelpunkt
  var centerTarget = MTLG.utilities.fbm.createTarget("centerTarget");
  centerTarget.x = 0.5;
  centerTarget.y = 0.5;

  var centerRotatedTarget = MTLG.utilities.fbm.createTarget("centerRotatedTarget");
  centerRotatedTarget.x = 0.5;
  centerRotatedTarget.y = 0.5;
  centerRotatedTarget.rotation = 180;

  //Multiple Choice Einführung
  var mcTut = MTLG.utilities.fbm.createContent("mcTut");
  mcTut.text = l("mc_Tut");
  mcTut.textX= 0.1;
  mcTut.textY = 0.5;
  mcTut.textWidth = 0.66;
  mcTut.textHeight = 0.9;
  mcTut.totalWidth = 0.6;
  mcTut.totalHeight = 0.4;
  mcTut.img = "img/task/confirmTut.png";
  mcTut.imgX = 1;
  mcTut.imgY = 0.5;
  mcTut.imgWidth = 0.34;
  mcTut.imgHeight = 0.7;
  mcTut.ratiolocked = true;

  var mcTarget = MTLG.utilities.fbm.createTarget("mcTarget");
  mcTarget.x = 0.5;
  mcTarget.y = 0.5;

  var mcTargetBot = MTLG.utilities.fbm.createTarget("mcTargetBot");
  mcTargetBot.x = 0.5;
  mcTargetBot.y = 0.75;

  var mcTargetTop = MTLG.utilities.fbm.createTarget("mcTargetTop");
  mcTargetTop.x = 0.5;
  mcTargetTop.y = 0.25;
  mcTargetTop.rotation = 180;

  var mcStyle = MTLG.utilities.fbm.createStyle("mcStyle");
  mcStyle.curve = 0.2;
  mcStyle.fontType = "Times New Roman";
  mcStyle.screenLocked = false;

  //Pairing Einführung
  var pairTut = MTLG.utilities.fbm.createContent("pairTut");
  pairTut.text = l("pair_Tut");
  pairTut.textX= 0.1;
  pairTut.textY = 0.5;
  pairTut.textWidth = 0.64;
  pairTut.textHeight = 0.9;
  pairTut.totalWidth = 0.6;
  pairTut.totalHeight = 0.4;
  pairTut.img = "img/task/pairTut.png";
  pairTut.imgX = 0.95;
  pairTut.imgY = 0.5;
  pairTut.imgWidth = 0.36;
  pairTut.imgHeight = 0.7;
  pairTut.ratiolocked = true;

  //Spieleinführung
  var gameGoal = MTLG.utilities.fbm.createContent("gameGoal");
  gameGoal.text = l("gameGoal");
  gameGoal.textX= 0.05;
  gameGoal.textY = 0.5;
  gameGoal.textWidth = 0.6;
  gameGoal.textHeight = 0.9;
  gameGoal.totalWidth = 0.6;
  gameGoal.totalHeight = 0.3;
  gameGoal.img = "img/gameboard/tutorial.png";
  gameGoal.imgX = 0.91;
  gameGoal.imgY = 0.5;
  gameGoal.imgWidth = 0.3;
  gameGoal.imgHeight = 0.8;

  //Hinweis zum Anklicken der Fragezeichen
  var gameHint = MTLG.utilities.fbm.createContent("gameHint");
  gameHint.text = l("gameHint");
  gameHint.textX= 0.05;
  gameHint.textY = 0.5;
  gameHint.textWidth = 0.6;
  gameHint.textHeight = 0.9;
  gameHint.totalWidth = 0.6;
  gameHint.totalHeight = 0.3;
  gameHint.img = "img/gameboard/tutHint.png";
  gameHint.imgX = 0.91;
  gameHint.imgY = 0.5;
  gameHint.imgWidth = 0.3;
  gameHint.imgHeight = 0.8;

  //Targets für die Spielkarten 
  var cardTutorialLeft = MTLG.utilities.fbm.createTarget("cardTargetLeft");
  cardTutorialLeft.x = 0.25;
  cardTutorialLeft.y = 0.5;
  cardTutorialLeft.rotation = 90;
  cardTutorialLeft.reqPointerRotation = -180;

  var cardTutorialRight = MTLG.utilities.fbm.createTarget("cardTargetRight");
  cardTutorialRight.x = 0.75;
  cardTutorialRight.y = 0.5;
  cardTutorialRight.rotation = -90;
  cardTutorialRight.reqPointerRotation = 180;

  var cardTutLeft = MTLG.utilities.fbm.createTarget("cardTutLeft");
  cardTutLeft.x = 0.25;
  cardTutLeft.y = 0.5;
  cardTutLeft.reqPointerRotation = -90;

  var cardTutRight = MTLG.utilities.fbm.createTarget("cardTutRight");
  cardTutRight.x = 0.75;
  cardTutRight.y = 0.5;
  cardTutRight.reqPointerRotation = 90;

};
