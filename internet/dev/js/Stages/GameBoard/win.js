/**
 * @Date:   2018-03-05T14:00:30+01:00
 * @Last modified time: 2018-03-07T10:39:45+01:00
 */

 var win_init = function()
 {
   // Die Funktion drawWin zeichnet den Win-screen
   areas = MTLG.createContainers(1);
   drawWin(areas[0]);
   MTLG.getStage().addChild(areas[0]);
 };

//Zeichne den Win-Screen
var drawWin = function(area)
{
    var stage = MTLG.getStage();

    //Hintergrund
    var background = MTLG.assets.getBitmap("img/mainMenu/mainBackground.png");
    var coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
    background.regX = background.getBounds().width*0.5;
    background.regY = background.getBounds().height*0.5;
    background.x = coordinates.xCoord;
    background.y = coordinates.yCoord;
    area.addChild(background);

    //Deko Konfetti
    var deco = MTLG.assets.getBitmap("img/gameboard/deco.png");
    coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
    deco.regX = deco.getBounds().width*0.5;
    deco.regY = deco.getBounds().height*0.5;
    deco.x = coordinates.xCoord;
    deco.y = coordinates.yCoord;
    area.addChild(deco);

    //Zurück zum Hauptmenü
    var backButton = MTLG.assets.getBitmap("img/options/backButton.png");
    backButton.regX = backButton.getBounds().width*0.5;
    backButton.regY = backButton.getBounds().height*0.5;
    backButton.x = area.getBounds().width*0.1;
    backButton.y = area.getBounds().height*0.2;
    backButton.on('click', function(event){
      MTLG.lc.goToMenu(); //Kehre wieder zurück zum Hauptmenü
    });
    area.addChild(backButton);

    //Gratulation
    var gratulationButton = MTLG.assets.getBitmap("img/options/optionsTitel.png");
    gratulationButton.regX = gratulationButton.getBounds().width*0.5;
    gratulationButton.regY = gratulationButton.getBounds().height*0.5;
    gratulationButton.x = area.getBounds().width*0.5;
    gratulationButton.y = area.getBounds().height*0.2;
    var templateGratulation = l("gratulation");
    var textGratulation = new createjs.Text(templateGratulation, '46px Courier New', 'white');
    textGratulation.regX = textGratulation.getBounds().width/2;
    textGratulation.regY = textGratulation.getBounds().height/2;
    textGratulation.x = area.getBounds().width*0.5;
    textGratulation.y = area.getBounds().height*0.2-5;
    var gratulationContainer = new createjs.Container();
    gratulationContainer.addChild(gratulationButton, textGratulation);
    area.addChild(gratulationContainer);

    //Roboter
    var winRoboter = MTLG.assets.getBitmapAbsoluteScale("img/gameboard/roboterWin"+playerTurn+".png", 0.6, 0.6);
    winRoboter.regX = winRoboter.getBounds().width*0.5;
    winRoboter.regY = winRoboter.getBounds().height*0.5;
    winRoboter.x = area.getBounds().width*0.41;
    winRoboter.y = area.getBounds().height*0.76;
    area.addChild(winRoboter);

    //Sprechblase mit Text
    var speechBubble = MTLG.assets.getBitmap("img/gameboard/speechBubble.png");
    speechBubble.regX = speechBubble.getBounds().width*0.5;
    speechBubble.regY = speechBubble.getBounds().height*0.5;
    speechBubble.x = area.getBounds().width*0.68;
    speechBubble.y = area.getBounds().height*0.48;
    var templateSpeech = l("speechWin_"+ MTLG.getPlayer(playerTurn));
    var textSpeech = new createjs.Text(templateSpeech, '50px Times New Roman', 'black');
    textSpeech.regX = textSpeech.getBounds().width/2;
    textSpeech.regY = textSpeech.getBounds().height/2;
    textSpeech.x = speechBubble.x;
    textSpeech.y = speechBubble.y-area.getBounds().height*0.05;
    var speechContainer = new createjs.Container();
    speechContainer.addChild(speechBubble, textSpeech);
    area.addChild(speechContainer);

};


/**
 * Die Funktion checkField überprüft, ob drawField das nächste Level sein soll
 * @param the current game state (an object containing only nextLevel in this case)
 * @return eine Zahl zwischen [0,1] welche die Möglichkeiten representiert
 */
var checkWin = function(gameState){
  if(!gameState){
    return 0; //Starte diese Spielebene nicht ohne Grund
  }
  if(!gameState.nextLevel){
    return 0; //Starte diese Spielebene nicht ohne Grund
  }
  if(gameState.nextLevel === 2.5){
    return 1;
  } else {
    return 0;
  }
}
