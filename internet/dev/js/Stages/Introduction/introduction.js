/**
 * @Date:   2017-12-16T18:04:11+01:00
 * @Last modified time: 2018-03-09T11:33:01+01:00
 */

 var introduction_init = function()
 {
   // Erstellt einen großen Container für die Stage.
   // Dieser Container stellt den Startpunkt der Spielstages dar.
   // Die Funktion drawIntroduction erstellt die Einführung mit Auswahl
   // der Anzahl an Spielern.
   areas = MTLG.createContainers(1);
   drawIntroduction(areas[0]);
   MTLG.getStage().addChild(areas[0]);
 };

//Zeichne die Auswahl der Spieleranzahl
var drawIntroduction = function(area)
{
  var stage = MTLG.getStage();

  //Lade Datenbank
  database();

  //Entfernt zunächste alle Spieler und resete Spiel
  MTLG.removeAllPlayers();
  resetParameter();

  //Hintergrund
  var background = MTLG.assets.getBitmap("img/mainMenu/mainBackground.png");
  var coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
  background.regX = background.getBounds().width*0.5;
  background.regY = background.getBounds().height*0.5;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;
  stage.addChild(background);

  //Anzahl der Spieler Text
  var templatePlayerNumber = l("player_number");
  var textPlayerNumber = new createjs.Text(templatePlayerNumber, '80px Times New Roman', 'white');
  textPlayerNumber.regX = textPlayerNumber.getBounds().width/2;
  textPlayerNumber.regY = textPlayerNumber.getBounds().height/2;
  textPlayerNumber.x =  area.getBounds().width*0.5;
  textPlayerNumber.y = area.getBounds().height*0.12;
  stage.addChild(textPlayerNumber);


  //Erstelle die vier möglichen Spielerauswahlen
  var onePlayer = new PlayerButton(area, "onePlayer", area.getBounds().width*0.35, area.getBounds().height*0.4);
  var twoPlayer = new PlayerButton(area, "twoPlayer", area.getBounds().width*0.65, area.getBounds().height*0.4);
  var threePlayer = new PlayerButton(area, "threePlayer", area.getBounds().width*0.35, area.getBounds().height*0.8);
  var twoPlayer = new PlayerButton(area, "fourPlayer", area.getBounds().width*0.65, area.getBounds().height*0.8);

};

//Konstruktor zur Auswahl der Anzahl der Spieler
function PlayerButton(area, numberOfPlayers, xCoor, yCoor){
    this.Container_constructor();

    this.setup(area, numberOfPlayers, xCoor, yCoor);
}

var p = createjs.extend(PlayerButton, createjs.Container);

p.setup = function(area, numberOfPlayers, xCoor, yCoor){

  //Lade das passende Bild für die Anzahl der Spieler
  var  imgPlayerButton = MTLG.assets.getBitmap("img/introduction/"+numberOfPlayers+".png");
  imgPlayerButton.regX = imgPlayerButton.getBounds().width*0.5;
  imgPlayerButton.regY = imgPlayerButton.getBounds().height*0.5;
  imgPlayerButton.x = xCoor;
  imgPlayerButton.y = yCoor;

  imgPlayerButton.on("click", function(event){
    //Füge die jeweils ausgewählte Anzahl an Spielern hinzu
    if(numberOfPlayers == "onePlayer"){
      MTLG.addPlayer(1);
      playerPosition = [-1];
      playerOffset = [0];
    } else
    if(numberOfPlayers == "twoPlayer"){
      MTLG.addPlayer(1);
      MTLG.addPlayer(2);
      playerPosition = [-1, -2];
      playerOffset = [0, 0];
    } else
    if(numberOfPlayers == "threePlayer"){
      MTLG.addPlayer(1);
      MTLG.addPlayer(2);
      MTLG.addPlayer(3);
      playerPosition = [-1, -2, -3];
      playerOffset = [0, 0, 0];
    } else
    if(numberOfPlayers == "fourPlayer"){
      MTLG.addPlayer(1);
      MTLG.addPlayer(2);
      MTLG.addPlayer(3);
      MTLG.addPlayer(4);
      playerPosition = [-1, -2, -3, -4];
      playerOffset = [0, 0, 0, 0];
    } else {console.log("Something with numberOfPlayers went wrong!")};
    //Beende Einführung und gehe zum nächsten Spielfeld
    MTLG.lc.levelFinished({nextLevel : 2, done : 0});
  });

  area.addChild(imgPlayerButton);
}

window.PlayerButton = createjs.promote(PlayerButton, "Container");

//Reset Funktion, setze alle Werte auf Ursprung zurück
function resetParameter(){
  //Spielfeld-Parameter
  fieldTaken = new Array(14).fill(0);
  playerCounter = 4;
  playerTurn = 0;

  //Aufgaben-Paramter
  //Multiple Choice
  solvedMCEasy = [];
  solvedMCHard = [];

  //Pairing Aufgaben
  solvedPairEasy = [];
  solvedPairHard = [];

  //Cloze Text
  solvedClozeEasy = [];
  solvedClozeHard = [];

}

/**
 * Die Funktion checkIntroduction überprüft, ob drawIntroduction das nächste Level sein soll
 * @param the current game state (an object containing only nextLevel in this case)
 * @return eine Zahl zwischen [0,1] welche die Möglichkeiten representiert
 */
var checkIntroduction = function(gameState){
  if(!gameState){
    return 1;   //Falls nichts vorgegeben, starte hier
  }
  if(!gameState.nextLevel){
    return 1;   //Falls nichts vorgegeben, starte hier 
  }
  if(gameState.nextLevel === 1){
    return 1;
  } else {
    return 0;
  }
}
