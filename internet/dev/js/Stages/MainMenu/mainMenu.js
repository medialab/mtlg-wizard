/**
 * @Date:   2017-12-13T14:13:14+01:00
 * @Last modified time: 2018-03-14T15:14:24+01:00
 */


//Zeichne das Hauptmenü
var drawMainMenu = function(){

  var stage = MTLG.getStage();

  if(MTLG.getPlayerNumber() >= 1){
    //Entfene alle Spieler aus MTLG
    MTLG.removeAllPlayers();
  }

  //Spieltitel
  var gameTitle = MTLG.assets.getBitmap("img/mainMenu/gameTitle.png");
  coordinates = {xCoord : options.width / 2, yCoord : options.height*0.2};
  gameTitle.regX = gameTitle.getBounds().width*0.5;
  gameTitle.regY = gameTitle.getBounds().height*0.5;
  gameTitle.x = coordinates.xCoord;
  gameTitle.y = coordinates.yCoord;

  //Hintergrund
  var background = MTLG.assets.getBitmap("img/mainMenu/mainBackground.png");
  coordinates = {xCoord : options.width/2, yCoord : options.height/2};
  background.regX = background.getBounds().width*0.5;
  background.regY = background.getBounds().height*0.5;
  background.scaleX = 1920/background.getBounds().width;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;

  //Start
  var startButton = MTLG.assets.getBitmap("img/mainMenu/startButton.png");
  coordinates = {xCoord : options.width/2, yCoord : options.height*0.5};
  startButton.regX = startButton.getBounds().width*0.5;
  startButton.regY = startButton.getBounds().height*0.5;
  startButton.x = coordinates.xCoord;
  startButton.y = coordinates.yCoord;
  var startContainer = new createjs.Container();
  startContainer.addChild(startButton);
  startContainer.on('click', function(event){
    MTLG.lc.goToLogin(); //Verlasse das Hauptmenü und wechsel zum Login
  });

  //Optionen
  var optionButton = MTLG.assets.getBitmap("img/mainMenu/optionButton.png");
  optionButton.regX = optionButton.getBounds().width*0.5;
  optionButton.regY = optionButton.getBounds().height*0.5;
  optionButton.visible = false;
  var optionButtonEng = MTLG.assets.getBitmap("img/mainMenu/optionButtonEng.png");
  optionButtonEng.regX = optionButtonEng.getBounds().width*0.5;
  optionButtonEng.regY = optionButtonEng.getBounds().height*0.5;
  optionButtonEng.visible = false;
  //Passe die Sprache des Option Buttons an, nicht über lang handler, weil die Schrift über PS besser aussah
  if(MTLG.lang.getLanguage() == "de"){
    optionButton.visible = true;
    optionButtonEng.visible = false;
  } else
  if(MTLG.lang.getLanguage() == "en"){
    optionButton.visible = false;
    optionButtonEng.visible = true;
  } else {
    console.log("Ungültige Sprache, setzte Sprache auf Englisch");
    optionButton.visible = false;
    optionButtonEng.visible = true;
  }

  var optionsContainer = new createjs.Container();
  coordinates = {xCoord : options.width / 2, yCoord : options.height*0.8};
  optionsContainer.x = coordinates.xCoord;
  optionsContainer.y = coordinates.yCoord;
  optionsContainer.addChild(optionButton, optionButtonEng);
  optionsContainer.on('click', function(event){
      MTLG.lc.levelFinished({nextLevel : 0.5, done : 0});
  });


  //Roboter
  var roboter = MTLG.assets.getBitmapAbsoluteScale("img/mainMenu/roboter.png", 0.6, 0.6);
  coordinates = {xCoord : options.width*0.15, yCoord : options.height*0.55};
  roboter.regX = roboter.getBounds().width*0.5;
  roboter.regY = roboter.getBounds().height*0.5;
  roboter.x = coordinates.xCoord;
  roboter.y = coordinates.yCoord;
  //Roboter mit grpßen Augen
  var roboterEye = MTLG.assets.getBitmapAbsoluteScale("img/mainMenu/roboterOpenEye.png", 0.6, 0.6);
  coordinates = {xCoord : options.width*0.15, yCoord : options.height*0.55};
  roboterEye.regX = roboterEye.getBounds().width*0.5;
  roboterEye.regY = roboterEye.getBounds().height*0.5;
  roboterEye.x = coordinates.xCoord;
  roboterEye.y = coordinates.yCoord;
  roboterEye.visible = false;
  //Wechsel die Augen bei einem Klick
  roboter.on("click", function(event){
      roboterEye.visible = true;
      roboter.visible = false;
  });
  roboterEye.on("click", function(event){
      roboterEye.visible = false;
      roboter.visible = true;
  });

  stage.addChild(background);
  stage.addChild(gameTitle);
  stage.addChild(startContainer);
  stage.addChild(optionsContainer);
  stage.addChild(roboter);
  stage.addChild(roboterEye);

  //Ruft die Funktionen zur Erstellung des Feedbacks auf
  createFeedback();

}
