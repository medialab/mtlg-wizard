/**
 * @Date:   2017-12-16T18:04:11+01:00
 * @Last modified time: 2018-03-12T11:12:38+01:00
 */

 var options_init = function()
 {
   // Die Funktion drawOptions zeichnet die Einstellungsmöglichkeiten
   areas = MTLG.createContainers(1);
   drawOptions(areas[0]);
   MTLG.getStage().addChild(areas[0]);
 };

//Zeichne das Einstellungsmenü
var drawOptions = function(area)
{
  var stage = MTLG.getStage();

  //Hintergrund
  var background = MTLG.assets.getBitmap("img/mainMenu/mainBackground.png");
  var coordinates = {xCoord : options.width / 2, yCoord : options.height/2};
  background.regX = background.getBounds().width*0.5;
  background.regY = background.getBounds().height*0.5;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;
  stage.addChild(background);

  //Titel des Menüs
  var optionsButton = MTLG.assets.getBitmap("img/options/optionsTitel.png");
  coordinates = {xCoord : options.width / 2, yCoord : options.height*0.2};
  optionsButton.regX = optionsButton.getBounds().width*0.5;
  optionsButton.regY = optionsButton.getBounds().height*0.5;
  optionsButton.x = coordinates.xCoord;
  optionsButton.y = coordinates.yCoord;
  var templateOptionsMenue = l("optionsMenu");
  var textOptionsMenue = new createjs.Text(templateOptionsMenue, '46px Courier New', 'white');
  textOptionsMenue.regX = textOptionsMenue.getBounds().width/2;
  textOptionsMenue.regY = textOptionsMenue.getBounds().height/2;
  textOptionsMenue.x = coordinates.xCoord;
  textOptionsMenue.y = coordinates.yCoord-5;
  var optionsMenueContainer = new createjs.Container();
  optionsMenueContainer.addChild(optionsButton, textOptionsMenue);
  stage.addChild(optionsMenueContainer);

  //Zurück Button
  var backButton = MTLG.assets.getBitmap("img/options/backButton.png");
  coordinates = {xCoord : options.width*0.1, yCoord : options.height*0.2};
  backButton.regX = backButton.getBounds().width*0.5;
  backButton.regY = backButton.getBounds().height*0.5;
  backButton.x = coordinates.xCoord;
  backButton.y = coordinates.yCoord;
  backButton.on('click', function(event){
    MTLG.lc.goToMenu(); //Kehre wieder zurück zum Hauptmenü
  });
  stage.addChild(backButton);

  //Confirm Button
  var confirmButton = MTLG.assets.getBitmap("img/options/confirmButton.png");
  coordinates = {xCoord : options.width*0.9, yCoord : options.height*0.9};
  confirmButton.regX = confirmButton.getBounds().width*0.5;
  confirmButton.regY = confirmButton.getBounds().height*0.5;
  confirmButton.x = coordinates.xCoord;
  confirmButton.y = coordinates.yCoord;
  confirmButton.on('click', function(event){
    MTLG.lc.goToMenu(); //Kehre wieder zurück zum Hauptmenü
  });
  stage.addChild(confirmButton);

  //Sprachauswahl
  var languageButton = MTLG.assets.getBitmap("img/options/buttonTitle.png");
  languageButton.regX = languageButton.getBounds().width*0.5;
  languageButton.regY = languageButton.getBounds().height*0.5;
  var templateLanguage = l("language");
  var textLanguage = new createjs.Text(templateLanguage, '46px Courier New', 'white');
  textLanguage.regX = textLanguage.getBounds().width/2;
  textLanguage.regY = textLanguage.getBounds().height/2;
  textLanguage.y = languageButton.y - 5;
  var languageContainer = new createjs.Container();
  languageContainer.addChild(languageButton, textLanguage);

  var engButton = MTLG.assets.getBitmap("img/options/engButton.png");
  engButton.regX = engButton.getBounds().width*0.5;
  engButton.regY = engButton.getBounds().height*0.5;
  engButton.x = languageContainer.x -130;
  engButton.y = languageContainer.y + 170;
  engButton.on("click", function(event){
    //Setze die Sprache und den Rahmen richtig
    MTLG.lang.setLanguage("en");
    languageFrame.x = engButton.x;
    languageFrame.y = engButton.y;
  });

  var deButton = MTLG.assets.getBitmap("img/options/deButton.png");
  deButton.regX = deButton.getBounds().width*0.5;
  deButton.regY = deButton.getBounds().height*0.5;
  deButton.x = languageContainer.x + 130;
  deButton.y = languageContainer.y + 170;
  deButton.on("click", function(event){
    MTLG.lang.setLanguage("de");
    languageFrame.x = deButton.x;
    languageFrame.y = deButton.y;
  });

  var languageFrame = MTLG.assets.getBitmap("img/options/frame.png");
  languageFrame.regX = languageFrame.getBounds().width*0.5;
  languageFrame.regY = languageFrame.getBounds().height*0.5;
  //Bestimme die Position des Rahmens entsprechend der Auswahl
  if(MTLG.lang.getLanguage() == "de"){
    languageFrame.x = deButton.x;
    languageFrame.y = deButton.y;
  } else
  if(MTLG.lang.getLanguage() == "en"){
    languageFrame.x = engButton.x;
    languageFrame.y = engButton.y;
  } else {
    console.log("Ungültige Sprache, setzte Sprache auf deutsch");
    languageFrame.x = deButton.x;
    languageFrame.y = deButton.y;
  }

  var languageElementsContainer = new createjs.Container();
  languageElementsContainer.addChild(languageContainer, engButton, deButton, languageFrame);
  languageElementsContainer.scaleX = 0.9;
  languageElementsContainer.scaleY = 0.9;
  languageElementsContainer.x = options.width*0.17;
  languageElementsContainer.y = options.height*0.5;
  stage.addChild(languageElementsContainer);

  //Touch
  var touchButton = MTLG.assets.getBitmap("img/options/buttonTitle.png");
  touchButton.regX = touchButton.getBounds().width*0.5;
  touchButton.regY = touchButton.getBounds().height*0.5;
  var templateTouch = l("touch");
  var textTouch = new createjs.Text(templateTouch, '46px Courier New', 'white');
  textTouch.regX = textTouch.getBounds().width/2;
  textTouch.regY = textTouch.getBounds().height/2;
  textTouch.y = touchButton.y - 5;
  var touchContainer = new createjs.Container();
  touchContainer.addChild(touchButton, textTouch);

  var yesButton = MTLG.assets.getBitmap("img/options/yesButton.png");
  yesButton.regX = yesButton.getBounds().width*0.5;
  yesButton.regY = yesButton.getBounds().height*0.5;
  yesButton.x = touchContainer.x -130;
  yesButton.y = touchContainer.y + 170;
  yesButton.on("click", function(event){
    //touchDevice ist die Variable zur Überprüfung ob Touchscreen vorhanden ist
    touchDevice = true;
    touchFrame.x = yesButton.x;
    touchFrame.y = yesButton.y;
  });

  var noButton = MTLG.assets.getBitmap("img/options/noButton.png");
  noButton.regX = noButton.getBounds().width*0.5;
  noButton.regY = noButton.getBounds().height*0.5;
  noButton.x = touchContainer.x + 130;
  noButton.y = touchContainer.y + 170;
  noButton.on("click", function(event){
    touchDevice = false;
    touchFrame.x = noButton.x;
    touchFrame.y = noButton.y;
  });

  var touchFrame = MTLG.assets.getBitmap("img/options/frame.png");
  touchFrame.regX = touchFrame.getBounds().width*0.5;
  touchFrame.regY = touchFrame.getBounds().height*0.5;
  if(touchDevice){
    touchFrame.x = yesButton.x;
    touchFrame.y = yesButton.y;
  } else {
    touchFrame.x = noButton.x;
    touchFrame.y = noButton.y;
  }

  var touchElementsContainer = new createjs.Container();
  touchElementsContainer.addChild(touchContainer, yesButton, noButton, touchFrame);
  touchElementsContainer.scaleX = 0.9;
  touchElementsContainer.scaleY = 0.9;
  touchElementsContainer.x = options.width*0.83;
  touchElementsContainer.y = options.height*0.5;
  stage.addChild(touchElementsContainer);

  //Schwierigkeitsgrad
  var difficultyButton = MTLG.assets.getBitmap("img/options/buttonTitle.png");
  difficultyButton.regX = difficultyButton.getBounds().width*0.5;
  difficultyButton.regY = difficultyButton.getBounds().height*0.5;
  var templateDifficulty = l("difficulty");
  var textDifficulty = new createjs.Text(templateDifficulty, '46px Courier New', 'white');
  textDifficulty.regX = textDifficulty.getBounds().width/2;
  textDifficulty.regY = textDifficulty.getBounds().height/2;
  textDifficulty.y = difficultyButton.y - 5;
  var difficultyContainer = new createjs.Container();
  difficultyContainer.addChild(difficultyButton, textDifficulty);

  //Setze taskLevel auf die entsprechende Schwierigkeitsstufe
  // 0 - gemixt | 1 - leichte | 2 - schwere

  var easyButton = MTLG.assets.getBitmap("img/options/easyButton.png");
  easyButton.regX = easyButton.getBounds().width*0.5;
  easyButton.regY = easyButton.getBounds().height*0.5;
  easyButton.x = difficultyContainer.x -220;
  easyButton.y = difficultyContainer.y + 170;
  easyButton.on("click", function(event){
    taskLevel = 1;
    difficultyFrame.x = easyButton.x;
    difficultyFrame.y = easyButton.y;
  });

  var hardButton = MTLG.assets.getBitmap("img/options/hardButton.png");
  hardButton.regX = hardButton.getBounds().width*0.5;
  hardButton.regY = hardButton.getBounds().height*0.5;
  hardButton.x = difficultyContainer.x + 220;
  hardButton.y = difficultyContainer.y + 170;
  hardButton.on("click", function(event){
    taskLevel = 2;
    difficultyFrame.x = hardButton.x;
    difficultyFrame.y = hardButton.y;
  });

  var mixButton = MTLG.assets.getBitmap("img/options/mixButton.png");
  mixButton.regX = mixButton.getBounds().width*0.5;
  mixButton.regY = mixButton.getBounds().height*0.5;
  mixButton.x = difficultyContainer.x;
  mixButton.y = difficultyContainer.y + 170;
  mixButton.on("click", function(event){
    taskLevel = 0;
    difficultyFrame.x = mixButton.x;
    difficultyFrame.y = mixButton.y;
  });

  var difficultyFrame = MTLG.assets.getBitmap("img/options/frame.png");
  difficultyFrame.regX = difficultyFrame.getBounds().width*0.5;
  difficultyFrame.regY = difficultyFrame.getBounds().height*0.5;
  if(taskLevel == 0){
    difficultyFrame.x = mixButton.x;
    difficultyFrame.y = mixButton.y;
  } else
  if(taskLevel == 1){
    difficultyFrame.x = easyButton.x;
    difficultyFrame.y = easyButton.y;
  } else {
    difficultyFrame.x = hardButton.x;
    difficultyFrame.y = hardButton.y;
  }

  var difficultyElementsContainer = new createjs.Container();
  difficultyElementsContainer.addChild(difficultyContainer, easyButton, hardButton, mixButton, difficultyFrame);
  difficultyElementsContainer.scaleX = 0.9;
  difficultyElementsContainer.scaleY = 0.9;
  difficultyElementsContainer.x = options.width*0.5;
  difficultyElementsContainer.y = options.height*0.5;
  stage.addChild(difficultyElementsContainer);

};


/**
 * Die Funktion checkIntroduction überprüft, ob drawIntroduction das nächste Level sein soll
 * @param the current game state (an object containing only nextLevel in this case)
 * @return eine Zahl zwischen [0,1] welche die Möglichkeiten representiert
 */
 var checkOptions = function(gameState){
   if(!gameState){
     return 0; //Starte diese Spielebene nicht ohne Grund
   }
   if(!gameState.nextLevel){
     return 0; //Starte diese Spielebene nicht ohne Grund
   }
   if(gameState.nextLevel === 0.5){
     return 1;
   } else {
     return 0;
   }
 }
