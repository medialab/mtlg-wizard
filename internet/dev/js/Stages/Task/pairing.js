/**
 * @Date:   2018-01-22T11:59:37+01:00
 * @Last modified time: 2018-03-16T12:16:38+01:00
 */

 //Überprüfe ob Aufgabentyp zum ersten Mal ausgeführt wird
var firstTimePair;

//Funktion zum Erstellen der Pairing Aufgabentyp
function pairing(area, level)
{
    //Ein Container mit allen Elemente einer Aufgaben
    var taskContainer = new createjs.Container();

    //Schwarze Abdeckung für das Spielfelder
    var blackCover = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
    blackCover.addEventListener('click', function(event){
        //Overlay welches clickevents abfängt und sonst nichts macht
    });

    //Setze mögliche Antworten zurück
    clearUserAnswer();

    if(level == "easy"){
        //Erzeugt eine zufäliige noch nicht ausgewählte Aufgabe
        if(counterPairEasy > 0){
            randomTaskFunction(pairingEasy, solvedPairEasy, "pairEasy");
        }else{
            //Falls alle Aufgaben bereits vorgekommen sind, wiederhole Aufgaben
            counterPairEasy =  pairingEasy.length;
            solvedPairEasy = [];
            randomTaskFunction(pairingEasy, solvedPairEasy, "pairEasy");
        }
    } else
    if(level == "hard"){
        //Erzeugt eine zufäliige noch nicht ausgewählte AUfgabe
        if(counterPairHard > 0){
            randomTaskFunction(pairingHard, solvedPairHard, "pairHard");
        }else{
            //Falls alle Aufgaben bereits vorgekommen sind, wiederhole Aufgaben
            counterPairHard =  pairingHard.length;
            solvedPairHard = [];
            randomTaskFunction(pairingHard, solvedPairHard, "pairHard");
        }
    } else {console.log("Level Parameter falsch");}

    //linke und rechte Box, welche die möglichen Paare beinhalten
    var pairingBoxL = MTLG.assets.getBitmap("img/task/pairingBox.png");
    pairingBoxL.regX = pairingBoxL.getBounds().width*0.5;
    pairingBoxL.regY = pairingBoxL.getBounds().height*0.5;
    pairingBoxL.x = area.getBounds().width*0.2;
    pairingBoxL.y = area.getBounds().height*0.5;

    var pairingBoxR = MTLG.assets.getBitmap("img/task/pairingBox.png");
    pairingBoxR.regX = pairingBoxR.getBounds().width*0.5;
    pairingBoxR.regY = pairingBoxR.getBounds().height*0.5;
    pairingBoxR.x = area.getBounds().width*0.8;
    pairingBoxR.y = area.getBounds().height*0.5;

    taskContainer.addChild(blackCover, pairingBoxL, pairingBoxR);

    //Übertrage die Lösungspaare in ein Set zum Abgleich
    var string;
    if(level == "easy"){
        string = pairingEasy[randomTask][8];
    } else {
        string = pairingHard[randomTask][8];
    }
    //Speichere die Sets der Musterlösung
    var correctSolution = [];
    var i = 0;
    while(i < string.length){
        if(string[i] == "("){
            var firstC = i;
            var lastC = string.indexOf(")", i);
            correctSolution[correctSolution.length] = makeSetOutOfString(string, firstC, lastC);
            i = lastC+1;
        } else {
            i++;
        }
    }

    //Erstelle die linken Boxen
    //Füge die ersten 4 Begriffe in ein einiges Array
    var leftText = [];
    if(level == "easy"){
        leftText = pairingEasy[randomTask].slice(0,4);
    } else {
        leftText = pairingHard[randomTask].slice(0,4);
    }
    var leftBox = [];
    for(var i = 0; i < 4; i++){
      leftBox[i] = createLeftBox(leftText[i]);
      leftBox[i].x = area.getBounds().width*0.2;
      var yPos;
      switch(i){
        case 0: yPos = 0.2; break;
        case 1: yPos = 0.4; break;
        case 2: yPos = 0.6; break;
        case 3: yPos = 0.8; break;
        default: yPos = -1;
      }
      leftBox[i].y = area.getBounds().height*yPos;
      taskContainer.addChild(leftBox[i]);
    }

    //Erstelle die rechten Boxen
    //Füge die nächsten 4 Begriffe in ein einiges Array
    var rightText = [];
    if(level == "easy"){
        rightText = pairingEasy[randomTask].slice(4,8);
    } else {
        rightText = pairingHard[randomTask].slice(4,8);
    }
    var rightBox = [];
    for(var i = 0; i < 4; i++){
      rightBox[i] = createRightBox(rightText[i]);
      rightBox[i].x = area.getBounds().width*0.8;
      var yPos;
      switch(i){
        case 0: yPos = 0.2; break;
        case 1: yPos = 0.4; break;
        case 2: yPos = 0.6; break;
        case 3: yPos = 0.8; break;
        default: yPos = -1;
      }
      rightBox[i].y = area.getBounds().height*yPos;
      taskContainer.addChild(rightBox[i]);
    }

  var pointsContainer = new createjs.Container();
  var rotation = 7;
  for(var i = 0; i < 8; i++){
    var x, y, id;
    //Passe die ID für Rotation an
    if(touchDevice == true){
      if(playerTurn == 1 || playerTurn == 2){
          id = rotation;
      } else { id = i;}
    } else { id = i;}
    //weise den Punkten die passenden Koordinaten zu
    switch(i){
      case 0:
        x = area.getBounds().width*0.3;
        y = area.getBounds().height*0.2;
        break;
      case 1:
        x = area.getBounds().width*0.3;
        y = area.getBounds().height*0.4;
        break;
      case 2:
        x = area.getBounds().width*0.3;
        y = area.getBounds().height*0.6;
        break;
      case 3:
        x = area.getBounds().width*0.3;
        y = area.getBounds().height*0.8;
        break;
      case 4:
        x = area.getBounds().width*0.7;
        y = area.getBounds().height*0.2;
        break;
      case 5:
        x = area.getBounds().width*0.7;
        y = area.getBounds().height*0.4;
        break;
      case 6:
        x = area.getBounds().width*0.7;
        y = area.getBounds().height*0.6;
        break;
      case 7:
        x = area.getBounds().width*0.7;
        y = area.getBounds().height*0.8;
        break;
      default: console.log("Fehler beim erstellen der Verbindungspunkte");
    }
    //Erstelle den Verbindungspunkt
    var point = new createjs.Shape().set({
        x:  x,
        y:  y,
        cursor: "pointer",
        name:"target",
        id: id.toString()
    });
    point.graphics.f(createjs.Graphics.getRGB("black"))
        .dc(0,0,20);
    point.on("mousedown", handlePress);
    pointsContainer.addChild(point);
    rotation -= 1;
  }

    //Bestätigungsbutton
    var semaphoreConfirm = true;
    var confirmButton = MTLG.assets.getBitmap("img/task/confirmButton.png");
    confirmButton.regX = confirmButton.getBounds().width*0.5;
    confirmButton.regY = confirmButton.getBounds().height*0.5;
    confirmButton.x = area.getBounds().width*0.55;
    confirmButton.y = area.getBounds().height*0.8;
    confirmButton.on("click", function(event){
      //Zeige alle richtigen Begriffe an mit grünem Flair
      for(i = 0; i < correctSolution.length; i++){
        for (let x of correctSolution[i]) {
            if(x<4){
                MTLG.utilities.fbm.giveFeedback(leftBox[x], "correctAnswer");
            } else {
                MTLG.utilities.fbm.giveFeedback(rightBox[x-4], "correctAnswer");
            }
        }
      }
      //Stellt sicher das die Taste nur einmal Betätigt werden kann
      if(semaphoreConfirm == true){
        semaphoreConfirm = false;
        //Vergleicht die Antwort mit der Lösung
        var check = compareAnswer(correctSolution, userAnswerSet);
        if(check){
            //Positiver Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "correctSound");
            setTimeout(function(){
                correctAnswer(area, level);
                area.removeChild(taskContainer);
                area.removeChild(pointsContainer);
                allLines.removeAllChildren();
            }, 2500);
        } else {
            //Zeige rotes Flair um die falsche Begriffe
            for(i = 0; i < userAnswerSet.length; i++){
              var check;
                for(var x of userAnswerSet[i]){
                    for(j = 0; j < correctSolution.length; j++){
                        if (correctSolution[j].has(x)){
                            check = true;
                            break;
                        }
                    }
                    if(!check){
                        if(x<4){
                            MTLG.utilities.fbm.giveFeedback(leftBox[x], "wrongAnswer");
                        } else {
                            MTLG.utilities.fbm.giveFeedback(rightBox[x-4], "wrongAnswer");
                        }
                    }
                }
            }
            //Negativer Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "wrongSound");
            setTimeout(function(){
                wrongAnswer(area,level);
                area.removeChild(taskContainer);
                area.removeChild(pointsContainer);
                allLines.removeAllChildren();
            }, 2500);
        }
      }
    });
    taskContainer.addChild(confirmButton);

    //Löschenbutton
    var deleteButton = MTLG.assets.getBitmap("img/task/deleteButton.png");
    deleteButton.regX = deleteButton.getBounds().width*0.5;
    deleteButton.regY = deleteButton.getBounds().height*0.5;
    deleteButton.x = area.getBounds().width*0.45;
    deleteButton.y = area.getBounds().height*0.8;
    deleteButton.on("click", function(event){
        allLines.removeAllChildren();
        clearUserAnswer();
    });
    taskContainer.addChild(deleteButton);

    //Anpassung des Registierpunkt und Koordianten des Contaienrs
    taskContainer.regX = taskContainer.getBounds().width*0.5;
    taskContainer.regY = taskContainer.getBounds().height*0.5;
    taskContainer.x = area.getBounds().width*0.5;
    taskContainer.y = area.getBounds().height*0.5;


    //Füge die Aufgabe der Area hinzu
    area.addChild(taskContainer);
    area.addChild(pointsContainer);

    //Richte das Aufgabenfeld zur richten Postion aus
    if(touchDevice == true){
      if(playerTurn == 1 || playerTurn == 2){
          taskContainer.rotation = 180;
      }else{
          taskContainer.rotation = 0;
      }
    }

    //Anleitung für die Aufgabe
    //Falls Aufgabe zum ersten Mal vom Spieler aufgerufen wurde, erstelle Anleitung
    if(firstTimePair > 0){
        firstTimePair -= 1;
        //Anleitung zum Aufgabentyp
        var blackCoverFB = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
        blackCoverFB.addEventListener('click', function(event){
            //Overlay welches clickevents abfängt und sonst nichts macht
        });
        area.addChild(blackCoverFB);

        //Entferne das schwarze Cover
        function removeCover (){
            area.removeChild(blackCoverFB);
        }

        //Timingobj für Pairing Einführung
        var pairTiming = MTLG.utilities.fbm.createTiming("pairTiming");
        pairTiming.totalDuration = 120;
        pairTiming.openDuration = 60;
        pairTiming.miniDuration = 40;
        pairTiming.requestable = false;
        pairTiming.closeable = true;
        pairTiming.minimizable = false;
        pairTiming.movable = false;
        pairTiming.closeCallback = function(){
          removeCover();};

        //Überprüfe ob Anleitung rotiert werden muss
        if(touchDevice == true){
            if(playerTurn == 1 || playerTurn == 2){
                MTLG.utilities.fbm.giveFeedback("centerRotatedTarget", "pairTut", "pairTiming", "mcStyle");
            }else{
                  MTLG.utilities.fbm.giveFeedback("centerTarget", "pairTut", "pairTiming", "mcStyle");
            }
        } else {
            MTLG.utilities.fbm.giveFeedback("centerTarget", "pairTut", "pairTiming", "mcStyle");
        }
    }

    /*---------------------------------*/

    //Funktion zum Erstellen der linken Boxen
    function createLeftBox(text){
          var leftBox = new createjs.Container();

          //Lade Bild der Box und zentrie diese
          var boxLeftBitmap = MTLG.assets.getBitmap("img/task/pairingLeft.png");
          boxLeftBitmap.x = - boxLeftBitmap.getBounds().width/2;
          boxLeftBitmap.y = - boxLeftBitmap.getBounds().height/2;;
          leftBox.addChild(boxLeftBitmap);

          //Erstelle den Text
          var boxLeftFontsize = 0.95*Math.sqrt(((boxLeftBitmap.getBounds().width*0.8) *0.95*(boxLeftBitmap.getBounds().height*0.4))/(text.length*0.5));
          var boxLeftText = new createjs.Text(text, boxLeftFontsize +"px Times New Roman", "black");
          boxLeftText.lineWidth = boxLeftBitmap.getBounds().width *0.8;
          boxLeftText.x = - boxLeftText.getBounds().width/2 - 70;
          boxLeftText.y = - boxLeftText.getBounds().height/2;
          leftBox.addChild(boxLeftText);

          return leftBox;
    }

    //Funktion zum Erstellen der rechten Boxen
    function createRightBox(text){
          var rightBox = new createjs.Container();

          //Lade Bild der Box und zentrie diese
          var boxRightBitmap = MTLG.assets.getBitmap("img/task/pairingRight.png");
          boxRightBitmap.x = - boxRightBitmap.getBounds().width/2;
          boxRightBitmap.y = - boxRightBitmap.getBounds().height/2;;
          rightBox.addChild(boxRightBitmap);

          //Erstelle den Text
          var boxRightFontsize = 0.95*Math.sqrt(((boxRightBitmap.getBounds().width*0.8) *0.95*(boxRightBitmap.getBounds().height*0.4))/(text.length*0.5));
          var boxRightText = new createjs.Text(text, boxRightFontsize +"px Times New Roman", "black");
          boxRightText.lineWidth = boxRightBitmap.getBounds().width *0.75;
          boxRightText.x = - boxRightText.getBounds().width/2 + 70;
          boxRightText.y = - boxRightText.getBounds().height/2;
          rightBox.addChild(boxRightText);

          return rightBox;
    }


    createjs.Ticker.addEventListener("tick", tick);
    stage.enableMouseOver();
}


//Werden benötigt um die Verbindungen zu speichern und entfernen zu können
var connection = null;
var linePlaceholder = null;
var allLines = new createjs.Container();

//Händelt das press event
function handlePress(event) {
    connection = new createjs.Shape().set({
        x:event.target.x,
        y:event.target.y,
        mouseEnabled:false,
        graphics: new createjs.Graphics().s("#00f").dc(0,0,50)
    });
    tempSet.clear();    //Lösche die Eingaben des letzten Sets
    addAnswerTemp(event.target.id);   //Füge die ID der aktuellen Verbindung hinzu
    stage.addChild(connection);
    stage.addEventListener("stagemousemove", drawLine);
    stage.addEventListener("stagemouseup", endDraw);
}

//Zeichne die Linie
function drawLine(event) {
    connection.graphics.clear()
        .setStrokeStyle(5)
        .s("#f00")
        .beginFill("#f00").dc(stage.mouseX-connection.x, stage.mouseY-connection.y, 10)
        .mt(0,0).lt(stage.mouseX-connection.x, stage.mouseY-connection.y);
}


//Behandelt das Ende des press Events
function endDraw() {
    //Speichere alle möglichen Targets ab
    var target, targets = stage.getObjectsUnderPoint(stage.mouseX, stage.mouseY);
    for (var i=0; i<targets.length; i++) {
        if (targets[i].name == "target") { target = targets[i]; break; }
    }
    //Überprüfe ob ein passendes Ziel gefunden wurde
    if (target != null) {
        //Zeichne neue Verbindung
        connection.graphics.clear()
          .setStrokeStyle(5)
          .s("black")
          .mt(0,0).lt(target.x-connection.x, target.y-connection.y);

        var x = target.x-connection.x;
        var y = target.y-connection.y;
        //Ersetzt Connection, um später alle Verbindungen entferen zu können
        linePlaceholder = new createjs.Shape().set({
          x: connection.x,
          y: connection.y,
          graphics: new createjs.Graphics()
        });

        linePlaceholder.graphics.clear()
            .setStrokeStyle(5)
            .s("black")
            .mt(0,0).lt(x, y);
        allLines.addChild(linePlaceholder);
        stage.addChild(allLines);
        stage.removeChild(connection);
        addAnswerTemp(target.id)
        addAnswerToArray(); //Füge die Verbindung den Antworten des Users hinzu 
    } else {
        stage.removeChild(connection);
    }

    stage.removeEventListener("stagemousemove", drawLine);
    stage.removeEventListener("stagemouseup", endDraw);
}


//Funktion die aus dem String, Sets zur Musterlösung erstellt
function makeSetOutOfString (string, firstC, lastC ){
      var set = new Set();
      var next;
      while(firstC < lastC){
          next = string.indexOf("," ,firstC+1);
          if(next == -1){     //Überpürft ob Ende erreicht ist
            next = lastC;
          }
          if(next > lastC){   //Ende der Klammer erreicht
              set.add(string.slice(firstC+1, next-1));
          } else {
              set.add(string.slice(firstC+1, next));
          }
          firstC = next;
      }
      if(firstC == 0){
          firstC = lastC +1;
      }

      return set;
}

//Funktion zum Verlgeich der Musterlösung mit dem des Users
function compareAnswer(correctArray, userArray){
    var check;
    for(i = 0; i < correctArray.length; i++){
        //Vergleicht ob ein Eintrag der Muserlösung in der vom User vorhanden ist
        for(j = 0; j < userArray.length; j++){
            check = eqSet(correctArray[i], userArray[j]);
            if(check == true){break}
        }
    //Falls ein Eintrag nicht enthalten ist, ist die Lösung des Users falsch
    if(check == false){ return check;}
    }
    //Alle richigen Antworten sind enthalten
    return check;
}


//Funktion zum Vergleich von zwei Sets
function eqSet(set1, set2) {
    if (set1.size !== set2.size) return false;
    for (var a of set1) if (!set2.has(a)) return false;
    return true;
}

//Überprüfe ob ein Element Teil eines Sets ist
function gotElement(set1, set2) {
    for (var a of set1) if (set2.has(a)) return true;
    return false;
}

//Array welche Sets enthält in denen die ausgewählten Antworten gespeichert sind
var userAnswerSet = [];

//Lösche alle Sets aus dem userAnswerSet Array
function clearUserAnswer(){
    var userLength = 4;
    for(i = 0; i < userLength; i++){
      var set = new Set();
      userAnswerSet[i] = set;
    }
}

//Speichert ausgwewählte Antworten zwischen
var tempSet = new Set();
function addAnswerTemp(targetID) {
    tempSet.add(targetID);
}

//Fügt die ausgewählten Antworten dem userAnswerSet hinzu
function addAnswerToArray(){
  var check;
  var freeSlot;
  //Durchläuft die bisherigen Verbindungen und überprüft ob Element schon vorhanden ist
  for(i = 0; i < userAnswerSet.length; i++){
      check = gotElement(userAnswerSet[i], tempSet);
      //Falls vorhanden füge an dieser Stelle restlichen Verbindungen hinzu
      if(check == true){
        freeSlot = i;
        const iterator = tempSet.entries();
        for (let entry of iterator) {
          userAnswerSet[freeSlot].add(entry[0]);
        }
        return;
      }
  }
  //Suche erste frei Stelle
  for(i = 0; i < userAnswerSet.length; i++){
      if(userAnswerSet[i].size == 0){
          freeSlot = i;
          break;
      }
  }
  const iterator = tempSet.entries();
  for (let entry of iterator) {
    userAnswerSet[freeSlot].add(entry[0]);
  }
  tempSet.clear();
}

function tick(event) {
    stage.update();
}
