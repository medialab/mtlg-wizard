/**
 * @Date:   2017-12-20T14:04:39+01:00
 * @Last modified time: 2018-03-14T16:24:53+01:00
 */

//Konstruktor für die Aufgaben
function Task(area, level, numberOfPlayers){
    this.Container_constructor();

    this.setup(area, level, numberOfPlayers);
}

var a = createjs.extend(Task, createjs.Container);

a.setup = function(area, level, numberOfPlayers)
{
    //Entferne Hinweise von Feedback von der Area
    MTLG.utilities.fbm.removeAllFeedback();

    // Im Folgenden wird eine zufälliger Aufgabentyp ausgewählt.
    // Dieser wird in Abhängigkeit von der Anzahl der Fragen, welcher dieser beinhaltet, bestimmt.
    // Umso mehr Fragen ein Aufgabentyp besitzt, desdo wahrscheinlicher kommt eine Frage dieses Aufgabentypen dran.
    // Falls man einen Aufgabentypen nicht im Spiel einsetzen möchte, kann man die länge des Aufgabentypen auf 0 setzen
    // Oder keine Frage in der Datenbank hinzufügen bzw. auskommentieren.

    //Speichere die Anzahl der Fragen von jedem Aufgabentyp und Schwierigkeitsgrad
    //Falls neuer Aufgabentyp hinzugefügt werden soll, um eine entsprechende Variable erweitern
    var mE = multipleChoiceEasy.length;
    var pE = pairingEasy.length;
    var cE = clozeEasy.length;
    var mH = multipleChoiceHard.length;
    var pH = pairingHard.length;
    var cH = clozeHard.length;

    //Speichert die gesamte Anzahl an Fragen
    var questionCount = 0;
    //Addiert die Anzahl der Fragen zum questionCount hinzu
    if(level == "easy"){
        questionCount += mE;
        questionCount += pE;
        questionCount += cE;
    } else {
        questionCount += mH;
        questionCount += pH;
        questionCount += cH;
    }

    // Wählt eine zufällige Zahl aus der gesamten Anzahl an Fragen aus
    var taskType = Math.floor(Math.random() * questionCount);

    // Wähle abhängig von der größere der Fragenkataloge einen Aufgabentypen aus
    // An dieser Stelle werden neue Aufgabentypen einbunden
    // Erweitere um eine neue Abfrage (taskType <(Anzahl vorheriger Fragen + neue Fragen))
    if(level == "easy"){
        if(taskType < mE){
            multipleChoice(area, level, numberOfPlayers);
        } else
        if(taskType < (mE + pE)){
            pairing(area, level);
        } else
        if(taskType < (mE + pE + cE)){
            clozeText(area, level);
        }
    } else {
        if(taskType < mH){
            multipleChoice(area, level, numberOfPlayers);
        } else
        if(taskType < (mH + pH)){
            pairing(area, level);
        } else
        if(taskType < (mH + pH + cH)){
            clozeText(area, level);
        }
    }

}

window.Task = createjs.promote(Task, "Container");


/*-----------------Hilfsfunktionen für Aufgaben und Bewegung der Charaktere-------------------*/

//Variablen zur Koordination der Aufgaben
var randomTask;
var counterMCEasy;
var counterMCHard;
var counterPairEasy;
var counterPairHard;
var counterClozeEasy;
var counterClozeHard;

function randomTaskFunction(taskList, solvedArray, taskTyp){
  randomTask = Math.floor(Math.random() * taskList.length);
  if(solvedArray.indexOf(randomTask) == -1){
    solvedArray.push(randomTask); //Füge Aufgabe dem Array der bereits aufgerufenen Aufgaben hinzu
    //Reduziere Counter von der jeweiligen Aufgabe
    if(taskTyp == "mcEasy"){
        counterMCEasy -= 1;
    } else
    if(taskTyp == "mcHard"){
        counterMCHard -= 1;
    } else
    if(taskTyp == "pairEasy"){
        counterPairEasy -= 1;
    } else
    if(taskTyp == "pairHard"){
        counterPairHard -= 1;
    } else
    if(taskTyp == "clozeEasy"){
        counterClozeEasy -= 1;
    } else
    if(taskTyp == "clozeHard"){
        counterClozeHard -= 1;
    }
  }else{
    //Wiederhole solange bis eine neue Aufgabe ausgewählt wurde
    randomTaskFunction(taskList, solvedArray, taskTyp);
  }
}

//Berechne Geschwindigkeit für die Bewegung der Figuren
function distanceTime(xEnd, yEnd){
  var distanceX2 = Math.pow(character[playerTurn].children[1].x - xEnd, 2);
  var distanceY2 = Math.pow(character[playerTurn].children[1].y - yEnd, 2);
  var distance = Math.sqrt(distanceX2+distanceY2);
  var time = 1000*distance/400;
  return time;
}

// Funktion zum Bewegen der Spielfiguren
function movePlayer(xCoor, yCoor, fieldPosition){
  if(touchDevice == true){
    //Bewegung zwischen Feld 0 und 1
    if(playerPosition[playerTurn] >= 0 && playerPosition[playerTurn] <= 1){
      //Überprüfe ob Feld frei ist
      if(fieldTaken[fieldPosition] == null || fieldTaken[fieldPosition] == 0){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+20), y:(yCoor), rotation: 90}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 1){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-28), y:(yCoor-48), rotation: 90}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 2){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-28), y:(yCoor+48), rotation: 90}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 3){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-30), y:(yCoor), rotation: 90}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      }else {
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor), rotation: 90}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
            console.log("Aller Felder besetzt");
      }
    } else
    //Bewegung zwischen Feld 2 bis 6
    if(playerPosition[playerTurn] >= 2 && playerPosition[playerTurn] <= 6){
      //Überprüfe ob Feld frei ist
      if(fieldTaken[fieldPosition] == null || fieldTaken[fieldPosition] == 0){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor+18), rotation: 180}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 1){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-48), y:(yCoor-34), rotation: 180}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 2){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+48), y:(yCoor-34), rotation: 180}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 3){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor-42), rotation: 180}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      }else {
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor), rotation: 180}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
            console.log("Aller Felder besetzt");
      }
    } else
    //Bewegung zwischen Feld 7 und 8
    if(playerPosition[playerTurn] >= 7 && playerPosition[playerTurn] <= 8){
      //Überprüfe ob Feld frei ist
      if(fieldTaken[fieldPosition] == null || fieldTaken[fieldPosition] == 0){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-20), y:(yCoor), rotation: 270}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 1){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+28), y:(yCoor+48), rotation: 270}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 2){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+28), y:(yCoor-48), rotation: 270}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 3){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+35), y:(yCoor), rotation: 270}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else {
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor), rotation: 270}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
            console.log("Aller Felder besetzt");
      }
    } else
    //Bewegung zwischen Feld 9 bis 13
    if(playerPosition[playerTurn] >= 9 && playerPosition[playerTurn] <= 13){
      //Überprüfe ob Feld frei ist
      if(fieldTaken[fieldPosition] == null || fieldTaken[fieldPosition] == 0){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor-12), rotation: 360}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 1){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor-48), y:(yCoor+40), rotation: 360}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 2){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor+48), y:(yCoor+40), rotation: 360}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      } else
      if(fieldTaken[fieldPosition] == 3){
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor+55), rotation: 360}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
      }else {
        createjs.Tween.get(character[playerTurn].children[1])
            .to({x:(xCoor), y:(yCoor), rotation: 360}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
            console.log("Aller Felder besetzt");
      }
    }
  } else {
    //Kein Touchscreen
    if(fieldTaken[fieldPosition] == null || fieldTaken[fieldPosition] == 0){
      createjs.Tween.get(character[playerTurn].children[1])
          .to({x:(xCoor), y:(yCoor-12)}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
    } else
    if(fieldTaken[fieldPosition] == 1){
      createjs.Tween.get(character[playerTurn].children[1])
          .to({x:(xCoor-48), y:(yCoor+40)}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
    } else
    if(fieldTaken[fieldPosition] == 2){
      createjs.Tween.get(character[playerTurn].children[1])
          .to({x:(xCoor+48), y:(yCoor+40)}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
    } else
    if(fieldTaken[fieldPosition] == 3){
      createjs.Tween.get(character[playerTurn].children[1])
          .to({x:(xCoor), y:(yCoor+52)}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
    }else {
      createjs.Tween.get(character[playerTurn].children[1])
          .to({x:(xCoor), y:(yCoor)}, distanceTime(xCoor, yCoor), createjs.Ease.linear.none);
          console.log("Aller Felder besetzt");
    }
  }
}

function correctAnswer(area, level)
{
    //Variable zur Anpassung der Schritte je nach Schwierigkeitsgrad
    var difficultAdder;
    if(level == "hard"){
      if(taskLevel == 2){
        difficultAdder = 1;
      } else {
        difficultAdder = 2;
      }
    } else{
      difficultAdder = 1;
    }

    var fieldPosition = 0;

    if(playerPosition[playerTurn] < 0){
      //Setzt die Figur auf die richtige Position
      switch(playerTurn){
        case 0:
          fieldPosition = -1 + difficultAdder;
          playerPosition[playerTurn] = fieldPosition;
          movePlayer(xField[fieldPosition], yField[fieldPosition], fieldPosition);
          fieldTaken[fieldPosition] += 1;
          playerOffset[playerTurn] += difficultAdder;
          break;
        case 1:
          fieldPosition =  1 + difficultAdder;
          playerPosition[playerTurn] = fieldPosition;
          movePlayer(xField[fieldPosition], yField[fieldPosition], fieldPosition);
          fieldTaken[fieldPosition] += 1;
          playerOffset[playerTurn] += difficultAdder;
          break;
        case 2:
          fieldPosition = 6 + difficultAdder;
          playerPosition[playerTurn] = fieldPosition;
          movePlayer(xField[fieldPosition], yField[fieldPosition], fieldPosition);
          fieldTaken[fieldPosition] += 1;
          playerOffset[playerTurn] += difficultAdder;
          break;
        case 3:
          fieldPosition = 8 + difficultAdder;
          playerPosition[playerTurn] = fieldPosition;
          movePlayer(xField[fieldPosition], yField[fieldPosition], fieldPosition);
          fieldTaken[fieldPosition] += 1;
          playerOffset[playerTurn] += difficultAdder;
          break;
        default:
          console.log("Fehler mit playerTurn");
      }
    } else
    //Zielbedingung
    if(playerPosition[playerTurn] > 0 && (playerOffset[playerTurn] >= 14 || (playerOffset[playerTurn] >= 13 && difficultAdder >= 2)) ){
      //Wechsel zum Win-Screen
      MTLG.lc.levelFinished({nextLevel : 2.5, done : 0});
    } else
    //Spieler befindet sich auf dem Feld
    if(playerPosition[playerTurn] >= 0){
      var newPositionX, newPositionY;
      fieldTaken[playerPosition[playerTurn]] -= 1;  //Gebe Platz auf dem Spielfeld frei
      //Überlauf der Figuren 2-4
      if(playerPosition[playerTurn] == 13){
        fieldPosition = -1 + difficultAdder;
        newPositionX = xField[fieldPosition];
        newPositionY = yField[fieldPosition];
        playerPosition[playerTurn] = fieldPosition;
      } else
      if(playerPosition[playerTurn] == 12 && difficultAdder == 2){
        fieldPosition = 0;
        newPositionX = xField[(0)];
        newPositionY = yField[(0)];
        playerPosition[playerTurn] = 0;
      } else {
        fieldPosition = playerPosition[playerTurn] + difficultAdder;
        newPositionX = xField[fieldPosition];
        newPositionY = yField[fieldPosition];
        playerPosition[playerTurn] += difficultAdder;
      }
      movePlayer(newPositionX, newPositionY, fieldPosition);
      fieldTaken[fieldPosition] += 1;
      playerOffset[playerTurn] += difficultAdder;
    } else {console.log("Position der Spielfigur unbekannt");}

    nextPlayer(area);
}

function wrongAnswer(area, level){
  nextPlayer(area);
}

//Zeigt an welcher Spieler als nächstes an der Reihe ist
var nextPlayer = function(area){
  if(playerTurn < MTLG.getPlayerNumber()-1){
    playerTurn = playerTurn + 1;
    if(playerTurn == 1){
      turnArrow.children[0].visible = false;
      turnArrow.children[1].visible = true;
      turnArrow.children[2].visible = false;
      turnArrow.children[3].visible = false;
      createjs.Tween.get(turnArrow)
          .to({rotation: 300}, 700, createjs.Ease.linear.none);
    } else
    if(playerTurn == 2){
      turnArrow.rotation = -60;
      turnArrow.children[0].visible = false;
      turnArrow.children[1].visible = false;
      turnArrow.children[2].visible = true;
      turnArrow.children[3].visible = false;
      createjs.Tween.get(turnArrow)
          .to({rotation: 60}, 700, createjs.Ease.linear.none);
    } else
    if(playerTurn == 3){
      turnArrow.children[0].visible = false;
      turnArrow.children[1].visible = false;
      turnArrow.children[2].visible = false;
      turnArrow.children[3].visible = true;
      createjs.Tween.get(turnArrow)
          .to({rotation: 120}, 700, createjs.Ease.linear.none);
    }
  } else {
    playerTurn = 0;
    turnArrow.children[0].visible = true;
    turnArrow.children[1].visible = false;
    turnArrow.children[2].visible = false;
    turnArrow.children[3].visible = false;
    createjs.Tween.get(turnArrow)
        .to({rotation: 240}, 700, createjs.Ease.linear.none);
  }

  var coverContainer = new createjs.Container();
  var cover = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
  var templatePlayer = l("player_"+ MTLG.getPlayer(playerTurn));
  var textCover = new createjs.Text(templatePlayer, '70px Arial', 'white');
  textCover.x = area.getBounds().width/2;
  textCover.y = area.getBounds().height/2;
  textCover.regX = textCover.getBounds().width/2;
  textCover.regY = textCover.getBounds().height/2;
  coverContainer.addChild(cover, textCover);

  if(touchDevice == true){
    if(playerTurn == 1 || playerTurn == 2){
        textCover.rotation = 180;
    }else{
        textCover.rotation = 0;
    }
  }
  //Erstelle die Hinweise auf dem Spielfeld neu 
  createTutorial();

  area.addChild(coverContainer);

  setTimeout(function(){
    objectFadeOut(coverContainer, 1000);
  }, 800);
}
