/**
 * @Date:   2018-01-22T11:59:37+01:00
 * @Last modified time: 2018-03-15T16:40:15+01:00
 */

 //Überprüfe ob Aufgabentyp zum ersten Mal ausgeführt wird
var firstTimeMC;

//Funktion zum Erstellen von Multiple Choice Aufgaben
function multipleChoice(area, level, numberOfPlayers)
{
    //Ein Container mit allen Elemente einer Aufgaben
    var taskContainer = new createjs.Container();

    //Schwarze Abdeckung für das Spielfelder
    var blackCover = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
    blackCover.addEventListener('click', function(event){
        //Overlay welches clickevents abfängt und sonst nichts macht
    });
    //Aufgabenbox, in welcher die Aufgaben angezeigt werden
    var taskBox;
    taskBox = MTLG.assets.getBitmap("img/task/TaskBox.png");
    taskBox.regX = taskBox.getBounds().width*0.5;
    taskBox.regY = taskBox.getBounds().height*0.5;
    taskBox.x = area.getBounds().width*0.5;
    taskBox.y = area.getBounds().height*0.5;

    //Füge die Elemente den Container hinzu
    taskContainer.addChild(blackCover, taskBox);

    //Zufällige Multiple Choice Aufgabe aus dem ausgewählten Schwierigkeitsgrad

    var boxA, boxB, boxC, boxD;

    if(level == "easy"){

      //Erzeugt eine zufäliige noch nicht ausgewählte Aufgabe
      if(counterMCEasy > 0){
        randomTaskFunction(multipleChoiceEasy, solvedMCEasy, "mcEasy");
      }else{
        //Falls alle Aufgaben bereits vorgekommen sind, wiederhole Aufgaben
        counterMCEasy =  multipleChoiceEasy.length;
        solvedMCEasy = [];
        randomTaskFunction(multipleChoiceEasy, solvedMCEasy, "mcEasy");
      }

      //Erstelle ein skalierten Aufgabentext
      var fontsize = 0.95*Math.sqrt(((taskBox.getBounds().width-20) *0.95*(taskBox.getBounds().height*0.3))/(multipleChoiceEasy[randomTask][1].length*0.5));
      if(fontsize >= 110){fontsize = 110};
      var taskText = new createjs.Text(multipleChoiceEasy[randomTask][1], fontsize +"px Times New Roman", "black");

    } else
    if(level == "hard"){

      //Erzeugt eine zufäliige noch nicht ausgewählte AUfgabe
      if(counterMCHard > 0){
        randomTaskFunction(multipleChoiceHard, solvedMCHard, "mcHard");
      }else{
        //Falls alle Aufgaben bereits vorgekommen sind, wiederhole Aufgaben
        counterMCHard =  multipleChoiceHard.length;
        solvedMCHard = [];
        randomTaskFunction(multipleChoiceHard, solvedMCHard, "mcHard");
      }

      //Erstelle ein skalierten Aufgabentext
      var fontsize = 0.95*Math.sqrt(((taskBox.getBounds().width -20) * (taskBox.getBounds().height*0.2))/(multipleChoiceHard[randomTask][1].length*0.5));
      if(fontsize >= 110){fontsize = 110};
      var taskText = new createjs.Text(multipleChoiceHard[randomTask][1], fontsize +"px Times New Roman", "black");

    } else {console.log("Level Parameter falsch");}



    //Erstelle den Aufgabentext
    taskText.lineWidth = taskBox.getBounds().width - 80;
    taskText.regX = taskBox.getBounds().width/2;
    taskText.regY = taskText.getBounds().height/2;
    taskText.x = area.getBounds().width*0.5 + 50;
    taskText.y = area.getBounds().height*0.21;


    //Erstelle die Antwortmöglichkeiten
    boxA = new BoxMC(randomTask, level, 2, area.getBounds().width*0.28, area.getBounds().height*0.52);
    boxB = new BoxMC(randomTask, level, 3, area.getBounds().width*0.72, area.getBounds().height*0.52);
    boxC = new BoxMC(randomTask, level, 4, area.getBounds().width*0.28, area.getBounds().height*0.78);
    boxD = new BoxMC(randomTask, level, 5, area.getBounds().width*0.72, area.getBounds().height*0.78);

    var boxArray = {
      "A": boxA,
      "B": boxB,
      "C": boxC,
      "D": boxD,
      "a": boxA,
      "b": boxB,
      "c": boxC,
      "d": boxD,
    };

    //Abfrage der Schwierigkeitsstufe
    if(level == "easy"){
      if(multipleChoiceEasy[randomTask][0] >= 1){boxA.visible = true;}
      if(multipleChoiceEasy[randomTask][0] >= 2){boxB.visible = true;}
      if(multipleChoiceEasy[randomTask][0] >= 3){boxC.visible = true;}
      if(multipleChoiceEasy[randomTask][0] == 4){boxD.visible = true;}
      if(multipleChoiceEasy[randomTask][0] > 4){console.log("Ungültige Anzahl an Antwortmöglichkeiten");}
    }else
    if(level == "hard"){
      if(multipleChoiceHard[randomTask][0] >= 1){boxA.visible = true;}
      if(multipleChoiceHard[randomTask][0] >= 2){boxB.visible = true;}
      if(multipleChoiceHard[randomTask][0] >= 3){boxC.visible = true;}
      if(multipleChoiceHard[randomTask][0] == 4){boxD.visible = true;}
      if(multipleChoiceHard[randomTask][0] > 4){console.log("Ungültige Anzahl an Antwortmöglichkeiten");}
    }


    //Kreuz beim Anklicken der Antwortmöglichkeiten
    var crossA = MTLG.assets.getBitmap("img/task/cross.png");
    crossA.regX = crossA.getBounds().width*0.5;
    crossA.regY = crossA.getBounds().height*0.5;
    crossA.visible = false;
    var crossB = MTLG.assets.getBitmap("img/task/cross.png");
    crossB.regX = crossB.getBounds().width*0.5;
    crossB.regY = crossB.getBounds().height*0.5;
    crossB.visible = false;
    var crossC = MTLG.assets.getBitmap("img/task/cross.png");
    crossC.regX = crossC.getBounds().width*0.5;
    crossC.regY = crossC.getBounds().height*0.5;
    crossC.visible = false;
    var crossD = MTLG.assets.getBitmap("img/task/cross.png");
    crossD.regX = crossD.getBounds().width*0.5;
    crossD.regY = crossD.getBounds().height*0.5;
    crossD.visible = false;

    taskContainer.addChild(taskText, boxA, boxB, boxC, boxD, crossA, crossB, crossC, crossD);

    //crossAnswer speichert die aktuell ausgewählte Antwort des Spielers
    var crossAnswer = [];
    var crossID;
    var semaphoreConfirm = true;

    boxA.on("click", function(event){
      if(semaphoreConfirm == true){
        if(crossA.visible == false){
          crossA.visible = true;
          crossAnswer.push("A");
        } else {
          crossA.visible = false;
          crossID = crossAnswer.indexOf("A");
          crossAnswer.splice(crossID, 1);
        }
          crossA.x = area.getBounds().width*0.28 - 320;
          crossA.y = area.getBounds().height*0.52;

          objectFadeIn(confirmButton, 400);
      }
    });

    boxB.on("click", function(event){
      if(semaphoreConfirm == true){
        if(crossB.visible == false){
          crossB.visible = true;
          crossAnswer.push("B");
        } else {
          crossB.visible = false;
          crossID = crossAnswer.indexOf("B");
          crossAnswer.splice(crossID, 1);
        }
          crossB.x = area.getBounds().width*0.72 - 320;
          crossB.y = area.getBounds().height*0.52;

          objectFadeIn(confirmButton, 400);
      }
    });

    boxC.on("click", function(event){
      if(semaphoreConfirm == true){
        if(crossC.visible == false){
          crossC.visible = true;
          crossAnswer.push("C");
        } else {
          crossC.visible = false;
          crossID = crossAnswer.indexOf("C");
          crossAnswer.splice(crossID, 1);
        }
        crossC.x = area.getBounds().width*0.28- 320;
        crossC.y = area.getBounds().height*0.78;

        objectFadeIn(confirmButton, 400);
      }
    });

    boxD.on("click", function(event){
      if(semaphoreConfirm == true){
        if(crossD.visible == false){
          crossD.visible = true;
          crossAnswer.push("D");
        } else {
          crossD.visible = false;
          crossID = crossAnswer.indexOf("D");
          crossAnswer.splice(crossID, 1);
        }
        crossD.x =area.getBounds().width*0.72 - 320;
        crossD.y =area.getBounds().height*0.78;

        objectFadeIn(confirmButton, 400);
      }
    });

    //Bestätigungstaste und Abgleich der Antwort mit der Lösung
    var confirmButton = MTLG.assets.getBitmap("img/task/confirm.png");
    confirmButton.regX = confirmButton.getBounds().width*0.5;
    confirmButton.regY = confirmButton.getBounds().height*0.5;
    confirmButton.x = area.getBounds().width*0.5;
    confirmButton.y = area.getBounds().height*0.65;
    confirmButton.alpha = 0;
    taskContainer.addChild(confirmButton);

    confirmButton.on("click", function(event){
      if(semaphoreConfirm == true){
        semaphoreConfirm = false;
        //Sortiere und füge die ausgewählten Antworten in einen Strin
        crossAnswer.sort();
        //Abgleich der Antwort mit der Lösung
        if(level == "easy"){
          if(crossAnswer.toString() == multipleChoiceEasy[randomTask][6]){
            for(i = 0; i < crossAnswer.length; i++){
            MTLG.utilities.fbm.giveFeedback(boxArray[crossAnswer[i]], "correctAnswer");
            }
            //Positiver Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "correctSound");
            setTimeout(function(){
              correctAnswer(area, level);
              area.removeChild(taskContainer);
            }, 2500);
          } else {
            //Entferne aus den ausgewählten Antworten die korrekte Lösung
            for(i = 0; i < multipleChoiceEasy[randomTask][6].length; i++){
              crossID = crossAnswer.indexOf(multipleChoiceEasy[randomTask][6].charAt(i));
              if(crossID != -1){crossAnswer.splice(crossID, 1);}
            }
            for(i = 0; i < crossAnswer.length; i++){
              MTLG.utilities.fbm.giveFeedback(boxArray[crossAnswer[i]], "wrongAnswer");
            }
            //Richtige Lösung soll angezeigt werden
            for(i = 0; i < multipleChoiceEasy[randomTask][6].length; i++){
              if(multipleChoiceEasy[randomTask][6].charAt(i) !== ","){
                MTLG.utilities.fbm.giveFeedback(boxArray[multipleChoiceEasy[randomTask][6].charAt(i)], "correctAnswer");
              }
            }

            //Negativer Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "wrongSound");
            setTimeout(function(){
              wrongAnswer(area,level);
              area.removeChild(taskContainer);
            }, 2500);
          }
        } else {
          if(crossAnswer.toString() == multipleChoiceHard[randomTask][6]){
            for(i = 0; i < crossAnswer.length; i++){
              MTLG.utilities.fbm.giveFeedback(boxArray[crossAnswer[i]], "correctAnswer");
            }
            //Positiver Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "correctSound");
            setTimeout(function(){
              correctAnswer(area, level);
              area.removeChild(taskContainer);
            }, 2500);
          } else {
            //Entferne aus den ausgewählten Antworten die korrekte Lösung
            for(i = 0; i < multipleChoiceHard[randomTask][6].length; i++){
              crossID = crossAnswer.indexOf(multipleChoiceHard[randomTask][6].charAt(i));
              if(crossID != -1){crossAnswer.splice(crossID, 1);}
            }
            for(i = 0; i < crossAnswer.length; i++){
            MTLG.utilities.fbm.giveFeedback(boxArray[crossAnswer[i]], "wrongAnswer");
            }
            //Richtige Lösung soll angezeigt werden
            for(i = 0; i < multipleChoiceHard[randomTask][6].length; i++){
              if(multipleChoiceHard[randomTask][6].charAt(i) !== ","){
                MTLG.utilities.fbm.giveFeedback(boxArray[multipleChoiceHard[randomTask][6].charAt(i)], "correctAnswer");
              }
            }
            //Negativer Sound Feedback
            MTLG.utilities.fbm.giveFeedback("centerTarget", "wrongSound");
            setTimeout(function(){
              wrongAnswer(area,level);
              area.removeChild(taskContainer);
            }, 2500);
          }
        }
      }
    });

    //Anpassung des Registierpunkt und Koordianten des Contaienrs
    taskContainer.regX = taskContainer.getBounds().width*0.5;
    taskContainer.regY = taskContainer.getBounds().height*0.5;
    taskContainer.x = area.getBounds().width*0.5;
    taskContainer.y = area.getBounds().height*0.5;
    //Richte das Aufgabenfeld zur richten Postion aus
    if(touchDevice == true){
      if(playerTurn == 1 || playerTurn == 2){
          taskContainer.rotation = 180;
      }else{
          taskContainer.rotation = 0;
      }
    }

    area.addChild(taskContainer);

    //Falls Aufgabe zum ersten Mal vom Spieler aufgerufen wurde, erstelle Anleitung
    if(firstTimeMC > 0){
        firstTimeMC -= 1;
        //Anleitung zum Aufgabentyp
        var blackCoverFB = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
        blackCoverFB.addEventListener('click', function(event){
            //Overlay welches clickevents abfängt und sonst nichts macht
        });
        area.addChild(blackCoverFB);

        //Entferne das Cover
        function removeCover (){
          area.removeChild(blackCoverFB);
        }

        //Lokas Timingobj für MC Feedback
        var mcTiming = MTLG.utilities.fbm.createTiming("mcTiming");
        mcTiming.totalDuration = 120;
        mcTiming.openDuration = 60;
        mcTiming.miniDuration = 40;
        mcTiming.requestable = false;
        mcTiming.closeable = true;
        mcTiming.minimizable = false;
        mcTiming.movable = false;
        mcTiming.closeCallback = function(){
          removeCover();};

        //Überprüfe ob Anleitung rotiert werden muss
        if(touchDevice == true){
            if(playerTurn == 1 || playerTurn == 2){
                MTLG.utilities.fbm.giveFeedback("centerRotatedTarget", "mcTut", "mcTiming", "mcStyle");
            }else{
                  MTLG.utilities.fbm.giveFeedback("mcTarget", "mcTut", "mcTiming", "mcStyle");
            }
        } else {
            MTLG.utilities.fbm.giveFeedback("mcTarget", "mcTut", "mcTiming", "mcStyle");
        }
    }
}

//Konstruktor für die Antwortmöglichkeiten von den Multiple Choice Aufgabenbox
function BoxMC(randomTask, level, answerNumber, xCoor, yCoor){
    this.Container_constructor();

    this.setup(randomTask, level, answerNumber, xCoor, yCoor);
}

var b = createjs.extend(BoxMC, createjs.Container);

b.setup = function(randomTask, level, answerNumber, xCoor, yCoor){

    //Ein Container für die Bestandteile
    var answerContainer = this;

    var mcBox = MTLG.assets.getBitmap("img/task/mcBox.png");
    mcBox.regX = mcBox.getBounds().width*0.5;
    mcBox.regY = mcBox.getBounds().height*0.5;
    mcBox.x = xCoor;
    mcBox.y = yCoor;

    var box = MTLG.assets.getBitmapScaled("img/task/box.png", 0.05);
    box.regX = box.getBounds().width*0.5;
    box.regY = box.getBounds().height*0.5;
    box.x = xCoor - 320;
    box.y = yCoor;

    //Text der Antwortmöglichkeiten
    if(level == "easy"){
      //Skkaliere den Text der Antworten
      var fontsize = 0.95*Math.sqrt(((mcBox.getBounds().width-50) *0.95*(mcBox.getBounds().height*0.48))/(multipleChoiceEasy[randomTask][answerNumber].length*0.5));
      if(fontsize >= 60){fontsize = 60};  //obere Grenze für die Schriftgröße
      var answer = new createjs.Text(multipleChoiceEasy[randomTask][answerNumber], fontsize +"px Times New Roman", "black");
    } else
    if(level == "hard"){
      var fontsize = 0.95*Math.sqrt(((mcBox.getBounds().width-50) *0.95*(mcBox.getBounds().height*0.48))/(multipleChoiceHard[randomTask][answerNumber].length*0.5));
      if(fontsize >= 60){fontsize = 60};  //obere Grenze für die Schriftgröße
      var answer = new createjs.Text(multipleChoiceHard[randomTask][answerNumber], fontsize +"px Times New Roman", "black");
    } else {
      console.log("Level Parameter falsch");
    }

    answer.lineWidth = mcBox.getBounds().width-250;
    answer.regX = mcBox.getBounds().width/2;
    answer.regY = answer.getBounds().height/2;
    answer.x = xCoor+200;
    answer.y = yCoor;

    this.addChild(mcBox, box, answer);
    //Erst sichtbar, wenn gefordert
    this.visible = false;
}

window.BoxMC = createjs.promote(BoxMC, "Container");
