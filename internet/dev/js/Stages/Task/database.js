/**
 * @Date:   2017-12-20T14:04:39+01:00
 * @Last modified time: 2018-03-16T12:19:13+01:00
 * Datenbank für die Inhalte aller Aufgaben
 */

 /*---------------------- Neuer Aufgabentyp ----------------------*/
 /* Speichert bereits gelöste Aufgaben
 var solvedNewEasy = [];
 var solvedNewHard = [];

 //Zweidimensionales Array zum speichern der Fragen
 var newEasy = [[]];
 var newHard = [[]];

 //Fragenkatalog in der richtigen Sprache in der database() speichern!
 */

/*---------------------- Multiple Choice ----------------------*/
//Speichert bereits gelöste Aufgaben
var solvedMCEasy = [];
var solvedMCHard = [];

//Eingabe der Fragen in das Array:
//[Anzahl der Antwortmöglichkeiten, Frage, Antwort A, Antwort B, Antwort C, Antwort D, Lösung]
// Falls mehrere Lösungen: "A,B" keine Leerzeichen dazwischen!!!
var multipleChoiceEasy = [[]];
var multipleChoiceHard = [[]];


/*-------------------------- Pairing --------------------------*/
//Speichert bereits gelöste Aufgaben
var solvedPairEasy = [];
var solvedPairHard = [];

//[Wort1, Wort2, Wort3, Wort4, Wort5, Wort6, Wort7, Wort8, Lösung]
var pairingEasy = [[]];
var pairingHard = [[]];

/*---------------------- Cloze Text ----------------------*/
//Speichert bereits gelöste Aufgaben
var solvedClozeEasy = [];
var solvedClozeHard = [];

//[Anzahl der Antwortmöglichkeiten, Lückentext, Lücke 1 Antwort, Lücke 2 Antwort, Lücke 3 Antwort, Falsche Antworten, Falsche Antwort]
// Lücke: _ nur ein Unterstrich!
var clozeEasy = [[]];
var clozeHard = [[]];



var database = function()
{
  //Deutsche Aufgaben
  if(MTLG.lang.getLanguage() == "de"){

      /*------Multiple Choice -----*/

      //Einfache Aufgaben
      multipleChoiceEasy = [
         [2, "Kann man das Internet (aus Versehen) kaputt machen?", "Ja, wenn zu viele Menschen gleichzeitig online sind kann das passieren", "Nein, nur wenn es weltweit keinen Strom mehr gäbe.", "null", "null", "B"],
         [2, "Was passiert bei einem Datenstau?", "Das Netzwerk mit seinen Leitungen, den „Datenautobahnen“, ihrer genutzten Bandbreite und die Kapazitäten der Rechenzentren sind für die Daten nicht ausreichend. Das Netzwerk ist überlastet. Die Daten können nicht wie gewohnt zügig zu ihrem Ziel transportiert werden. Es kommt zu Wartezeiten, oder sogar zu Ausfällen.", "Die Daten reihen sich in einer Warteschlange einund werden nach und nach abgearbeitet", "null", "null", "A"],
         [3, "Achtung Datenstau!", "Optimiere deine Hardware innerhalb des Netzwerkes", "Integriere außerdem ein Speichernetzwerk um deine Daten während des Transports abzusichern und so Datenverlust vorzubeugen", "Der Datenstau löst sich von selber", "null", "A,B"],
         [3, "Dein Laptop (PC, Rechner) ist plötzlich verlangsamt, es scheint manchmal so, als würde er von jemand anderem gesteuert werden!", "Starte ein Virus-Erkennungs-Programm (Anti-Virensoftware/ Anti-Virusprogramm)", "Entferne anschließend die schädliche Maleware!", "Den Rechner neustarten, damit werde alle Probleme gelöscht", "null", "A,B"],
         [3, "Was ist ein Trojanisches Pferd?", "Ein Schadprogramm (böses Programm), das sich als nützliches Programm tarnt", "Ein Programm, dass die Festplatte ausspioniert und so Daten, die nicht für alle bestimmt sind (Kontodaten, persönliche Daten), an Kriminelle weiterleitet", "Ein Programm, welches das Internet verbessert", "null", "A,B"],
         [4, "Welcher Fehlercode erscheint bei einer nicht gefundenen Internetseite?", "666", "1337", "007", "404", "D"],
         [4, "Welchen Internetbrowser gibt es nicht?", "Safari", "Firefox", "Chrome", "Pinguin", "D"],
         [2, "Wie nennt man das Betrachten von Internetseiten?", "googeln", "surfen", "null", "null", "B"],
         [2, "Was bedeutet die Abkürzung AFK?", "Away from keyboard", "Another failed key", "null", "null", "A"],
         [3, "Kevin Mackenzie erfand im April 1979 das Emoticon. Wie sah sein Prototyp aus?", "Ein Smiley mit großen Augen 8)", "Ein Smiley mit einem lächelnden Mund :)", "Ein Smiley mit herausgestreckter Zunge :P", "null", "C"],
         [3, "Am 23. April 2005 um 20:27 Uhr wurde das erste Video auf YouTube hochgeladen. Was zeigt es?", "Den Zoobesuch eines YouTube-Gründers", "Einen kurzen Ausschnitt der YouTube-Geburtstagsfeier", "Einen der Gründer des Videoportals, wie er seinen Großvater interviewt", "null", "A"],
         [4, "Du kriegst mit, dass jemand in einem Forum, indem du aktiv bist, regelmäßig gemobbt wird. Was tust du?", "Nicht mein Problem, und wenn ich mich da einmische, werde ich vielleicht selbst gemobbt", "Wer gemobbt wird, hat das meistens auch verdient, da steuere ich auch mal einen lockeren Spruch bei", "Bevor die anderen denken, dass ich auf der Seite des Trottels stehe, steuere ich auch regelmäßig einen Spruch bei", "Ich spreche mit dem Betroffenen und biete meine Unterstützung an. Wenn ich alleine nicht gegen die Mobber ankomme, schalte ich Erwachsene ein", "D"],
         [3, "Ein Internetfreund von dir möchte sich mit dir treffen. Würdest du dich mit ihm treffen?", "Auf jeden Fall, das ist doch total spannend", "Das würde ich erst einmal mit meinen Eltern besprechen, mich nur an einem öffentlichen Ort treffen und eventuell einen Freund mitnehmen", "Ja, wenn ich ihn lange genug kenne und er einen netten Eindruck macht, ist das kein Problem", "null", "B"],
         [3, "Worauf solltest du achten, wenn du eine neue App auf dem Smartphone installierst?", "Wenn die App cool ist und von vielen benutzt wird, ist sie auch sicher", "Ich muss auf gar nichts achten, die Apps werden alle vom Hersteller genau geprüft", "Ich lese mir die Kommentare dazu durch und checke, welche Berechtigung die App auf meinem Handy haben will", "null", "C"],
      ];

     //Schwierige Aufgaben
      multipleChoiceHard = [
          [3, "Du suchst nach einer Routenplaner-App für dein Smartphone. Du findest eine App sehr vielversprechend und willst sie heruntladen. Die App möchte jedoch Zugriffsrechte auf deinen Standort, deine Kontaktliste und deinen Kalender haben. Lädst du die App trotzdem herunter?", "Ja, alle drei Zugriffsrechte werden für die Funktionen eines guten Routenplaners benötigt.", "Nein, da eine Routenplaner-App keinen Zugriff auf den Kalender und die Kontaktliste benötigt.", "Nein, da eine Routenplaner-App keinen Zugriff auf den Standort und den Kalender benötigt", "null", "B"],
      ];

      /*------Pairing Aufgaben -----*/

      //Einfache Aufgaben
      pairingEasy = [
          ["Pinguin", "Computer", "Chrome", " ", "Browser", "Hardware", "Software", "Mediumware", "(1,5),(2,4,6)"],
      ];

      //Schwierige Aufgaben
      pairingHard = [
          ["Mandarine", "Pizza", "Erdberre", "Pommes", " ", "Fast Food", "Obst", " ", "(0,2,6),(1,3,5)"],
      ];


      /*------Cloze Text -----*/
      //Leere Datenbank, da Lückentexte noch nicht einsetzbar sind
      //Einfache Aufgaben
      clozeEasy = [
          //[2, "Hallo Welt, das hier ist ein _ Text. Es sollten keine _ auftreten! Ansonsten kann man leider nichts daran machen. end", "Beispiel", "Probleme", "nichts", "Falsch", "Vielleicht"]
      ];

      //Schwere Aufgaben
      clozeHard = [
          //[3, "Hallo Welt, das hier ist  ein Beispiel. Hoffentlich klappt _ alles ohne größere _ ! Aber wenn nicht kann man daran leider _ än.", "Lückentext", "Fehler", "nichts", "Falsch", "Vielleicht"]
      ];

  } else
  //Englische Aufgaben
  if(MTLG.lang.getLanguage() == "en"){

    /*------Multiple Choice -----*/

    //easy tasks
    multipleChoiceEasy = [
       [4, "english easy assignment", "first answer", "second answer", "thrid answer", "fourth answer", "A"],
          ];

   //difficult tasks
    multipleChoiceHard = [
         [4, "english hard assignment", "first answer", "second answer", "thrid answer", "fourth answer", "A,B"],
    ];

    /*------Pairing Aufgaben -----*/

    //easy tasks
    pairingEasy = [
        ["eng 0", "eng 1", "eng 2", "eng 3", "eng 4", "eng 5", "eng 6", "eng 7", "(1,5),(2,4,6)"],
    ];

    //difficult tasks
    pairingHard = [
        ["eng a", "eng b", "eng c", "eng d", "eng e", "eng f", "eng g", "eng h", "(1,5),(2,4,6)"],
    ];


    /*------Cloze Text -----*/
    //empty database because cloze text doesnt work
    //easy tasks
    clozeEasy = [
        //[1, "English _ text", "easy", "fault", "null", "wrong", "easy"]
    ];

    //Schwere Aufgaben
    clozeHard = [
        //[1, "English _ text", "hard", "fault", "null", "wrong", "hard"]
    ];

  }

  //Speichere die Anzahl der Fragen von dem Aufgabentypen
  counterMCEasy =  multipleChoiceEasy.length;
  counterMCHard = multipleChoiceHard.length;
  counterPairEasy =  pairingEasy.length;
  counterPairHard = pairingHard.length;

}
