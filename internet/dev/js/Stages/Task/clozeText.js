/**
 * @Date:   2018-01-22T11:59:37+01:00
 * @Last modified time: 2018-03-16T12:16:38+01:00
 */


//Funktion zum Erstellen von Lückentext Aufgabentyp
function clozeText(area, level)
{
    //Ein Container mit allen Elemente einer Aufgaben
    var taskContainer = new createjs.Container();

    //Schwarze Abdeckung für das Spielfelder
    var blackCover = MTLG.assets.getBitmap("img/gameboard/blackCover.png");
    blackCover.addEventListener('click', function(event){
        //Overlay welches clickevents abfängt und sonst nichts macht
    });
    //Aufgabenfelder
    var clozeBox = MTLG.assets.getBitmap("img/task/clozeBox.png");
    clozeBox.regX = clozeBox.getBounds().width*0.5;
    clozeBox.regY = clozeBox.getBounds().height*0.5;
    clozeBox.x = area.getBounds().width*0.35;
    clozeBox.y = area.getBounds().height*0.5;
    var choiceBox = MTLG.assets.getBitmap("img/task/choiceBox.png");
    choiceBox.regX = choiceBox.getBounds().width*0.5;
    choiceBox.regY = choiceBox.getBounds().height*0.5;
    choiceBox.x = area.getBounds().width*0.83;
    choiceBox.y = area.getBounds().height*0.5;

    taskContainer.addChild(blackCover, clozeBox, choiceBox);

    area.addChild(taskContainer);

    //Ruft die Funktion auf, welche den Lückentext erzeugt
    randomTask = 0;
    createClozeText(clozeEasy[randomTask][1]);

    /*---------------------------------*/

    //Erstelle den Lückentext
    function createClozeText(text){
      //Erstelle einen Container für alle Text Elemente
      var allTextContainer = new createjs.Container();
      var allGapContainer = new createjs.Container();
      var allAnswerContainer  = new createjs.Container();

      //Bestimme Position der Lücken
      var gap1 = text.indexOf("_");
      var gap2 = text.indexOf("_", (gap1+1));
      var gap3 = text.indexOf("_", (gap2+1));
      var endText = text.length-1;
      //console.log(gap1, gap2, gap3, endText);

      //Aktuelle Position im String
      var currentPos = 0;

      //Berechne max fontsize
      var fontsize = 0.95*Math.sqrt(((clozeBox.getBounds().width-20) *0.95*(clozeBox.getBounds().height*0.8))/(text.length*0.5));
      var indexEnd = gap1;

      //Erstelle Text bis zur ersten Lücke
      while(currentPos <= (gap1-1)){
        currentPos = createRow(text, fontsize, currentPos, gap1);
      }

      //Position des Ersten Textes
      allTextContainer.children[0].x = area.getBounds().width*0.07;
      allTextContainer.children[0].y = area.getBounds().height*0.1;
      //Positioniere den Text
      placeText(1);

      //Letztes Textelement
      var lastTextChild = allTextContainer.children.length-1;

      //Erstelle GapBox
      var boxPos = createGapBox();

      //Überprüfe ob zweite Lücke benötigt wird
      if(gap2 > -1){
        //Erstelle Text für die Zeile der Box
        if(boxPos == 1){
            currentPos = gap1+2;  //Entferne das _ vom Text
            indexEnd = gap1 + 30;
            currentPos = createRow(text, fontsize, currentPos, indexEnd, allGapContainer.children[0].getBounds().width);
            allTextContainer.children[lastTextChild+1].x =  allGapContainer.children[0].x + allGapContainer.children[0].getBounds().width + 10;
            allTextContainer.children[lastTextChild+1].y =  allGapContainer.children[0].y;
            indexEnd = gap2;
        } else {
          currentPos = gap1+2;  //Entferne das _ vom Text
          indexEnd = gap2;      //Setze das Ende bis zur nächsten Lücke
        }

        //Erstelle Text bis zu 2 Lücke
        while(currentPos <= (gap2-1)){
          currentPos = createRow(text, fontsize, currentPos, gap2);
        }

        //Positioniere Text und erstelle Lücke
        if(boxPos == 0){
            console.log("Lücke ist in der oberen Zeile ");
            allTextContainer.children[lastTextChild+1].x =  area.getBounds().width*0.07;
            allTextContainer.children[lastTextChild+1].y =  allTextContainer.children[lastTextChild].y + allTextContainer.children[0].getMeasuredLineHeight();
        }
        placeText(lastTextChild+2);
        boxPos = createGapBox();
      }

      //Überprüfe ob dritte Lücke benötigt wird
      if(gap3 > -1){
        //Erstelle Text für die Zeile der Box
        if(boxPos == 1){
            currentPos = gap2+2;  //Entferne das _ vom Text
            indexEnd = gap2 + 30;
            currentPos = createRow(text, fontsize, currentPos, indexEnd, allGapContainer.children[0].getBounds().width);
            allTextContainer.children[lastTextChild+1].x =  allGapContainer.children[0].x + allGapContainer.children[0].getBounds().width + 10;
            allTextContainer.children[lastTextChild+1].y =  allGapContainer.children[0].y;
            indexEnd = gap3;
        } else {
          currentPos = gap2+2;  //Entferne das _ vom Text
          indexEnd = gap3;      //Setze das Ende bis zur nächsten Lücke
        }

        //Erstelle Text bis zu 3 Lücke
        while(currentPos <= (gap3-1)){
          currentPos = createRow(text, fontsize, currentPos, gap3);
        }

        //Positioniere Text und erstelle Lücke
        if(boxPos == 0){
            allTextContainer.children[lastTextChild+1].x =  area.getBounds().width*0.07;
            allTextContainer.children[lastTextChild+1].y =  allTextContainer.children[lastTextChild].y + allTextContainer.children[0].getMeasuredLineHeight();
        }
        placeText(lastTextChild+2);
        boxPos = createGapBox();
      }

      if(currentPos < endText){

        lastTextChild = allTextContainer.children.length-1;

        currentPos += 2;  //Entferne _ vom Text
        indexEnd = endText;
        while(currentPos <= endText-4){
          currentPos = createRow(text, fontsize, currentPos, endText);
        }
        placeText(lastTextChild+1);
      }

      //Setzte alle Bounds von den Lücken korrekt
      for(i = 0; i < allGapContainer.children.length; i++){
          allGapContainer.children[i].setBounds(allGapContainer.children[i].x,allGapContainer.children[i].y, allGapContainer.children[i].getBounds().width, allGapContainer.children[i].getBounds().height );
      }

      //Erstelle die Antwortmöglichkeiten
      for(i = 2; i < clozeEasy[randomTask].length ; i++){
        if(clozeEasy[randomTask][i] !== null){
          createAnswerBox(clozeEasy[randomTask][i], choiceBox.x, choiceBox.y )
        }
      }


      //Füge Text Container zu area hinzu
      area.addChild(allTextContainer);
      area.addChild(allGapContainer);
      area.addChild(allAnswerContainer);

      //Funktion die eine passende Zeile erstellt
      function createRow(text, fontsize, indexStart, gapIndex, boxWidth){
        //Entfernt das letzte Wort des zu betrachtenden Strings
        indexEnd = text.lastIndexOf(" ", indexEnd-1);
        //Erstelle ein temporaeren Teilstring
        var testText =  text.substring(indexStart, indexEnd);
        //Erzeuge ein neues Textobjekt fuer die Zeile mit dem Teilstring
        var rowText = new createjs.Text(testText, fontsize +"px Times New Roman", "black");
        if(boxWidth == null){
            rowText.lineWidth = clozeBox.getBounds().width - 90;
        }else{
            rowText.lineWidth = clozeBox.getBounds().width - boxWidth;
        }
        //Ueberpruefe ob das Textobjekt in eine Zeile passt
        if(rowText.getMeasuredHeight() > rowText.getMeasuredLineHeight()){
            //Falls nein, rufe die Funktion rekusiv auf und beginne erneut
            return createRow(text, fontsize, indexStart, gapIndex);
        } else {
            //Falls ja, verschiebe den Index und fuege die Zeile hinzu
            indexStart += (indexEnd-indexStart+1);
            indexEnd = gapIndex;
            allTextContainer.addChild(rowText);
            return indexStart;
        }
      }

      //Funktion zum erstellen von Lücken
      function createGapBox(){
        //Skaliere Box in Abhängigkeit von dem Text
        var scaleBox = ((1.07*fontsize)/110);
        var boxPos;
        var gapBox = MTLG.assets.getBitmapAbsoluteScale("img/task/fillBox.png", 1, scaleBox );
        //Bounds müssen manuell gesetzt werden
        gapBox.setBounds(0,0, gapBox.getBounds().width, gapBox.getBounds().height*scaleBox);
        var lastChild = allTextContainer.children.length-1;
        if(allTextContainer.children[lastChild].getBounds().width <= 625){
            gapBox.x = allTextContainer.children[lastChild].x + allTextContainer.children[lastChild].getBounds().width + 10;
            gapBox.y = allTextContainer.children[lastChild].y;
            boxPos = 0; //Box passte in die voherige Zeile
        }else{
            gapBox.x = area.getBounds().width*0.07;
            gapBox.y = allTextContainer.children[lastChild].y + allTextContainer.children[0].getMeasuredLineHeight();
            boxPos = 1; //Box ist in einer neuen Zeile
        }
        allGapContainer.addChild(gapBox);
        //console.log("boxPos: "+ boxPos);
        return boxPos;
      }

      //Positionierung der Textbausteine
      function placeText(startElement){
        for(i = startElement; i < allTextContainer.children.length; i++){
          allTextContainer.children[i].x = allTextContainer.children[0].x;
          allTextContainer.children[i].y = allTextContainer.children[(i-1)].y + allTextContainer.children[i].getMeasuredLineHeight();
        }
      }

      //Erstelle die Antort Boxen
      function createAnswerBox(text, xCoor, yCoor){
        //Skaliere Box in Abhängigkeit von dem Text
        var scaleBox = ((1.07*fontsize)/110);
        var answerContainer = new createjs.Container();

        //Erstelle Box der Antwort
        var answerBox = MTLG.assets.getBitmapAbsoluteScale("img/task/answerBox.png", 1, scaleBox);
        answerBox.setBounds(0,0, answerBox.getBounds().width, answerBox.getBounds().height*scaleBox);
        answerBox.x = - answerBox.getBounds().width/2;
        answerBox.y = - answerBox.getBounds().height/2;;
        answerContainer.addChild(answerBox);

        //Erstelle den Text
        var answerFontsize = 0.95*Math.sqrt(((answerBox.getBounds().width-10) *0.95*(answerBox.getBounds().height*0.48))/(text.length*0.5));
        var answerText = new createjs.Text(text, answerFontsize +"px Times New Roman", "black");
        answerText.lineWidth = answerBox.getBounds().width - 100;
        answerText.x = - answerText.getBounds().width/2;
        answerText.y = - answerText.getBounds().height/2;
        answerContainer.addChild(answerText);

        answerContainer.x = xCoor;
        answerContainer.y = yCoor;
        answerContainer.setBounds(xCoor, yCoor, answerBox.getBounds().width, answerBox.getBounds().height);
        allAnswerContainer.addChild(answerContainer);

        var xoffset, yoffset;
         answerContainer.on("mousedown", function(evt) {
           xoffset = evt.stageX-answerContainer.x;
           yoffset = evt.stageY-answerContainer.y;
         });
         answerContainer.on("pressmove", function(evt) {
             answerContainer.x = evt.stageX-xoffset;
             answerContainer.y = evt.stageY-yoffset;
             stage.update();
             /*
            for(i = 0; i < allGapContainer.children.length; i++){
              if(intersect(evt.currentTarget, allGapContainer.children[i])){
               evt.currentTarget.alpha=0.2;
             }else{
               evt.currentTarget.alpha=1;
             }
            }
            */
         });
         answerContainer.on("pressup", function(evt) {
           for(i = 0; i < allGapContainer.children.length; i++){
             if(intersect(evt.currentTarget, allGapContainer.children[i])){
               answerContainer.x = allGapContainer.children[i].x + answerBox.getBounds().width/2;
               answerContainer.y = allGapContainer.children[i].y + answerBox.getBounds().height/2;
               answerContainer.alpha = 1;
               stage.update(evt);
           }
         }
        });


        //Überprüft ob Objekte sich überschneiden
        function intersect(obj1, obj2){
          var objBounds1 = obj1.getBounds().clone();
          var objBounds2 = obj2.getBounds().clone();
          var objTest = obj1.globalToLocal(objBounds2.x, objBounds2.y);

          var h1 = -(objBounds1.height / 2 + objBounds2.height);
          var h2 = objBounds2.height / 2;
          var w1 = -(objBounds1.width / 2 + objBounds2.width);
          var w2 = objBounds2.width / 2;

          if(objTest.x > w2 || objTest.x < w1) return false;
          if(objTest.y > h2 || objTest.y < h1) return false;

          return true;
        }

      }
    }

}
