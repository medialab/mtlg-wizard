/**
 * @Date:   2017-12-13T14:13:14+01:00
 * @Last modified time: 2018-03-05T14:10:15+01:00
 */



/*
 * These variables should always be present and used if necessary.
 */
var options, //game options read from manifest files
  gameData, //game state, level info...
  areas, //Information about user areas.
  sharedArea, //Free area left
  players, //An array containing information about the players.
  firstRun; //A bool remembering if this is the first time the game is run

/*
 * These variables are game specific.
 */
var readyCounter;//Counts the number of players that are ready

//It is useful to redefine MTLG.lang.getString
//Write l(key) to get the corresponding string.
//The lang module is loaded after spiel, so the var is defined in init.
var l;

/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function(pOptions){
  //Initialisiere das Spiel
  //Set default var values
  setupVars(pOptions);

  // Registiere das Hauptmenü
  MTLG.lc.registerMenu(drawMainMenu);
  // Registiere die einzelnen Spielebenen mithilfe des Life Cycle Manager
  MTLG.lc.registerLevel(options_init, checkOptions);
  MTLG.lc.registerLevel(introduction_init, checkIntroduction);
  MTLG.lc.registerLevel(field_init, checkField);
  MTLG.lc.registerLevel(win_init, checkWin);

  // Definiere l als Abkürzung für Übersetzungen
  l = MTLG.lang.getString;

  //Initialisierung ist fertig
  console.log("Game Init function called");
}

// Übergebe dem MTLG Framewirk die Initialisierungfunktion
MTLG.addGameInit(initGame);


//Setzt Parameter für das Spiel
var setupVars = function(pOptions)
{
    stage = MTLG.getStage();
    options = pOptions;
    gameData = {};
    readyCounter = 0;
    areas = [];
    sharedArea = {};
    players = []; //This can be populated in the first level after MTLG.goToLogin was called.
    firstRun = true;
};
