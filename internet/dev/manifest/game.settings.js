/**
 * @Author: thiemo
 * @Date:   2017-11-17T14:40:50+01:00
 * @Last modified by:   thiemo
 * @Last modified time: 2017-11-20T16:45:47+01:00
 */

window.MTLG = (function(m) {
  var settings = {
    default: {
      // add default configuration for the game
    },
    all: {
      // add all settings to get it in a settings menu
    }

  }

  m.loadSettings(settings);
  return m;
})(window.MTLG);
