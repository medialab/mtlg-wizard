/**
 * @Date:   2017-12-13T14:13:14+01:00
 * @Last modified time: 2017-12-16T18:00:54+01:00
 */



var MTLG=(function(m){
  var options = {
    "width":1920, //game width in pixels
    "height":1080, //game height in pixels
    "languages":["de","en"], //Supported languages. First language should be the most complete.
    "countdown":180, //idle time countdown
    "fps":"60", //Frames per second
    "playernumber":4, //Maximum number of supported players
    "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  }

  m.loadOptions(options);
  return m;
})(MTLG);
