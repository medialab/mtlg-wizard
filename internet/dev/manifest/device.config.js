/**
 * @Date:   2017-12-13T14:13:14+01:00
 * @Last modified time: 2018-01-04T18:31:29+01:00
 */



var MTLG=(function(m){
  var deviceOptions = {
    "zoll":27, //screen size in inches
    "mockLogin":true, //set to true to skip login procedure
    "mockNumber":1, //number of generated fake logins if mockLogin is true
    "loginMode":6, //Determines the login procedure for userLogin
    "language":"de", //Highest priority language setting, device dependant
  }

  m.loadOptions(deviceOptions);
  return m;
})(MTLG);
