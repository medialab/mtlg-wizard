/**
 * @Author: thiemo
 * @Date:   2017-12-01T11:54:38+01:00
 * @Last modified by:
 * @Last modified time: 2018-03-07T14:15:04+01:00
 */



window.MTLG = (function(m) {
  var modulsMTLG = {
    // add blocklist if needed default is load || if listed and set to 0 then module is bocked
    tabulaEvents: 0,
    tangibleModul: 0
  }

  m.loadModulsMTLG(modulsMTLG);
  return m;
})(window.MTLG);
